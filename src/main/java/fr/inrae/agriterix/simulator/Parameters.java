package fr.inrae.agriterix.simulator;

import fr.inrae.agriterix.simulator.dynamics.Dynamics;
import java.util.List;

public class Parameters {
    
    private final int rngSeedIndex;
    private List<Dynamics> dynamics;
    private String configPath;

    public Parameters(int rngSeedIndex, List<Dynamics> dynamics, String configPath) {
        this.rngSeedIndex = rngSeedIndex;
        this.dynamics = dynamics;
        this.configPath = configPath;
        for (Dynamics dynamic : dynamics) {
            dynamic.setConfigPath(configPath);
            dynamic.initParameters();
        }
    }

    public int getRngSeedIndex() {
        return rngSeedIndex;
    }

    public List<Dynamics> getDynamics() {
        return dynamics;
    }
    
}