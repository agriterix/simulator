package fr.inrae.agriterix.simulator;

import org.apache.commons.lang3.mutable.MutableInt;
import umontreal.ssj.randvar.NormalGen;
import umontreal.ssj.rng.MRG32k3a;

import java.util.*;
import java.util.stream.Collectors;

public class SimulationContext {

    private MutableInt timestep;
    private final MRG32k3a randomStream;
    private int farmerCount;

    public SimulationContext(MutableInt timestep, int rngSeedIndex) {
        this.timestep = timestep;
        this.randomStream = new MRG32k3a();
        randomStream.setSeed(new long[]{12345, 12345, 12345, 12345, 12345, 12345});
        for (int i = 0; i < rngSeedIndex; i++) {
            randomStream.resetNextSubstream();
        }
        farmerCount = 0;
    }

    public int setIdFarmer() {
        farmerCount++;
        return farmerCount;
    }

    public int getTimestep() {
        return timestep.getValue();
    }

    /**
     * This method returns a double picked out following a Uniform law between 0
     * and 1 with 0 and 1 excluded (can't be picked out)
     */
    public double nextDouble() {
        return randomStream.nextDouble();
    }


    public float nextFloat() {
        Double value = randomStream.nextDouble();
        return value.floatValue();
    }

    /**
     * (From MRG32k3a doc)
     * Calls nextDouble once to create one integer between i and j.
     * This method always uses the highest order bits of the random number.
     * It should be overridden if a faster implementation exists for the specific generator.
     * @param i the smallest possible returned integer
     * @param j the largest possible returned integer
     * @return a random integer between i and j
     */
    public int nextInt(int i, int j) {
        return randomStream.nextInt(i, j);
    }

    /**
     * Pick out an object in the values of a map, according to the probabilities
     * stored in the keys
     */
    public <T> T nextMapObjectWithDistributionInKeys(SortedMap<Double, T> map) {
        double rando = nextDouble();
        double value = map.firstKey();
        Iterator<Double> keysIterator = map.keySet().iterator();
        double sum = keysIterator.next();
        while (keysIterator.hasNext()) {
            //FIXIT problem with below : stop on the next value even if next one is closer.
            sum += value;
            if (sum >= rando) {
                break;
            }
            value = keysIterator.next();
        }
        return map.get(value);
    }

    /**
     * Pick out an object in the values of a map, according to the probabilities
     * stored in the values
     * @param map HashMap of probabilities sum of all values must be 1.
     */
    public <T> T nextMapObjectWithDistributionInKeysBis(Map<T, Double> map) {
        // Sorting the map
        map = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        double sum = 0;
        // scaling map values on [0, 1]
        for (Map.Entry<T, Double> entry : map.entrySet()) {
            map.put(entry.getKey(), entry.getValue() + sum);
            sum = entry.getValue();
        }
        double rand = nextDouble();
        Iterator<T> keysIterator = map.keySet().iterator();
        T returned = keysIterator.next();
        while (keysIterator.hasNext()) {
            if (rand <= map.get(returned)) {
                break;
            }
            returned = keysIterator.next();
        }
        return returned;
    }


    /**
     * Pick out a float following a Normal law
     */
    public double pickOutNormalLaw(double m, double std) {
        return NormalGen.nextDouble(randomStream, m, std);
    }

    /**
     * Picks out randomly an object in the given list.
     *
     * @param <T>
     * @param objects
     * @return
     */
    public <T> T nextObject(List<T> objects) {
        return nextObject(objects, false);
}

    /**
     * Picks out randomly an object in the given list.
     *
     * @param <T>
     * @param objects
     * @param remove if true, the returned object is removed from the list
     * @return null if the list is null or empty.
     */
    public <T> T nextObject(List<T> objects, boolean remove) {
        if (objects == null || objects.isEmpty()) {
            return null;
        }
        if (remove) {
            return objects.remove(nextInt(0, objects.size() - 1));
        } else {
            return objects.get(nextInt(0, objects.size() - 1));
        }
    }

    public boolean nextBoolean() {
        return nextDouble() < 0.5;
    }

}
