package fr.inrae.agriterix.simulator;

public class Patch {

    private transient Farm farm;
    private GeoArea geoArea;
    private Float surface;
    private CulturalPractice practice;
    private Production production;
    private Float yield;
    private int id;

    public static class Production {
        private Product product;
        private Float value;

        public Production(Product product, Float value) {
            this.product = product;
            this.value = value;
        }

        public Product getProduct() {
            return product;
        }

        public Float getValue() {
            return value;
        }
    }

    public Patch(GeoArea geoArea, Float surface, CulturalPractice practice, Product product, Integer id) {
        this.geoArea = geoArea;
        this.surface = surface;
        this.production = new Production(product, 0.f);
        this.practice = practice;
        this.yield = this.computeYield();
        this.id = id;
    }

    public Patch(GeoArea geoArea, Float surface, CulturalPractice practice, Product product, Farm farm, Integer id) {
        this(geoArea, surface, practice, product, id);
        this.farm = farm;
        farm.addPatch(this);
        this.id = id;
    }

    /**
     * estimate Yield regarding culture type and practice.
     *
     * @return a factor by surface unit.
     */
    public Float computeYield() {
        return this.getCulturalPractice().getYield() * this.getProduction().getProduct().getYield();
    }

    public Farm getFarm() {
        return farm;
    }

    public Stocks produce() {
//        // Bio matching
        if (this.getCulturalPractice() == CulturalPractice.O && !production.product.getTags().contains(Tag.B)) {
            production.product.addTag(Tag.B);
        }
//        // Regular matching
        if (this.getCulturalPractice() == CulturalPractice.I || this.getCulturalPractice() == CulturalPractice.S
            && production.getProduct().getTags().contains(Tag.B)) {
            production.product.removeTag(Tag.B);
        }
        production = new Production(production.product, surface * yield);
        return new Stocks(production.getProduct(), production.getValue());
    }

    public Production getProduction() {
        return production;
    }

    public void setProduction(Production production) {
        this.production = production;
    }

    /**
     * Set the farm where this patch and add the patch into the farm.
     *
     * @param farm farm selected
     */
    public void setFarm(Farm farm) {
        this.farm = farm;
        farm.addPatch(this);
    }

    public void setFarmSimple(Farm farm) {
        this.farm = farm;
    }




    public GeoArea getGeoArea() {
        return geoArea;
    }

    /**
     * Set the geoArea where this patch and add the patch into the geoArea object.
     *
     * @param geoArea geoArea selected
     */
    public void setGeoArea(GeoArea geoArea) {
        this.geoArea = geoArea;
        geoArea.getPatches().add(this);
    }

    public Float getSurface() {
        return surface;
    }

    public CulturalPractice getCulturalPractice() {
        return practice;
    }
    public void setCulturalPractice(CulturalPractice culturalPractice) {
        this.practice = culturalPractice;
    }

    public int getId() {
        return id;
    }

    public Float getYield() {
        return yield;
    }
}