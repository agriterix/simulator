package fr.inrae.agriterix.simulator;

/**
 * Enum to specify time
 */
public enum TimeUnit {
    DAY(1),
    MONTH(12),
    YEAR(365);

    private final int value;

    TimeUnit(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
