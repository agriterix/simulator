package fr.inrae.agriterix.simulator;

import java.util.*;

public class Product {

    private String label;
    private final Unit unit;
    //@XStreamImplicit
    private List<Tag> tags;
    private Float yield;

    public Product(String label, Unit unit) {
        this.label = label;
        this.unit = unit;
        this.tags = new ArrayList<>();
        this.yield = 1f;
    }

    public Product(String label, Unit unit, Float yield) {
        this.label = label;
        this.unit = unit;
        this.tags = new ArrayList<>();
        this.yield = yield;
    }

    public Product(String label, Unit unit, List<Tag> tags) {
        this.label = label;
        this.unit = unit;
        this.tags = new ArrayList<>();
        this.tags.addAll(tags);
        this.yield = 1f;
    }

    public Product(String label, Unit unit, List<Tag> tags, Float yield) {
        this.label = label;
        this.unit = unit;
        this.tags = new ArrayList<>();
        this.tags.addAll(tags);
        this.yield = yield;
    }

    @Override
    public String toString() {
        return label + " " + unit + " " + tags;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public Unit getUnit() {
        return unit;
    }

    public Float getYield() {
        return yield;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
        tags = new ArrayList<>(new HashSet<>(tags));
        Collections.sort(tags);
    }

    public void removeTag(Tag tag) {
        tags.remove(tag);
        tags = new ArrayList<>(new HashSet<>(tags));
        Collections.sort(tags);
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (getClass() != o.getClass())
            return false;
        return this.toString().equals(o.toString());
    }

    public boolean equalsTagLess(Product product) {
        return this.getLabel().equals(product.getLabel()) && this.getUnit().equals(product.getUnit());
    }

}
