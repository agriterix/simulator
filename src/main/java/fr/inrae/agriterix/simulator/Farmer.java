package fr.inrae.agriterix.simulator;

public class Farmer {

    private int age;
    private Farm farm;
    private int id;

    public Farmer(int age, Farm farm, int id) {
        this.age = age;
        this.farm = farm;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Farm getFarm() {
        return farm;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

    public int getAge() {
        return age;
    }

    public void aging() {
        age++;
    }
}