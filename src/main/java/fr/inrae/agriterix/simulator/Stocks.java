package fr.inrae.agriterix.simulator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


// Because I was tired of handling Map<Product, Float> everywhere...
public class Stocks {

    // TODO add option for Map<Product, Integer> for animals stocks.
    private Map<Product, Float> stocks;

    public Stocks() {
        stocks = new HashMap<>();
    }

    public Stocks(Stocks existing) {
        this.stocks = existing.getStocks();
    }

    public Stocks(Product product, Float value) {
        this.stocks = new HashMap<>();
        this.stocks.put(product, value);
    }

    public Map<Product, Float> getStocks() {
        return stocks;
    }

    public Float get(Product product) {
        return stocks.get(product);
    }

    public boolean containsKey(Product product) {
        return stocks.containsKey(product);
    }

    public void empty(Product product) {
        if (stocks.containsKey(product)) {
            stocks.put(product, 0f);
        }
    }

    public void clear() {
        stocks.clear();
    }

    public void add(Product product, Float quantity) {
        if (stocks.containsKey(product)) {
            stocks.put(product, stocks.get(product) + quantity);
        } else {
            stocks.put(product, quantity);
        }
    }

    public void set(Product product, Float quantity) {
        stocks.put(product, quantity);
    }
    
    public void subtract(Product product, Float quantity) {
        if (stocks.get(product) <= 0) {
            stocks.put(product, 0.f);
        } else {
            stocks.put(product, stocks.get(product) - quantity);
        }
    }

    public void addStock(Stocks toAdd) {
        toAdd.getStocks().forEach((k,v) -> stocks.merge(k, v, Float::sum));
    }

    public Set<Map.Entry<Product, Float>> entrySet() {
        return stocks.entrySet();
    }

    @Override
    public String toString() {
        return stocks.toString();
    }
}
