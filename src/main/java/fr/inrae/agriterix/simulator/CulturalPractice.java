package fr.inrae.agriterix.simulator;

public enum CulturalPractice {
    S("sustainable", 1f),
    O("organic", 0.75f),
    I("intensive", 1.25f);

    private final String label;
    private final Float yield;

    CulturalPractice(String label, Float yield) {
        this.label = label;
        this.yield = yield;
    }

    public String getLabel() {
        return label;
    }

    public Float getYield() {
        return yield;
    }
}
