package fr.inrae.agriterix.simulator;

import java.util.Map;

public class Livestock {

    private Stocks animals;
    private Ratio ratio;

    public Livestock(Map<Product, Float> required, Map<Product, Float> produced) {
        this.animals = new Stocks();
        this.ratio = new Ratio(required, produced);
    }

    public Livestock(Map<Product, Float> required, Map<Product, Float> produced, Stocks animals) {
        this.animals = animals;
        this.ratio = new Ratio(required, produced);
    }

    // For now we consider that all animal's food are supplied internally, otherwise productions can be updated w/
    // other external products
    public Stocks produce(Stocks productions) {
        return ratio.transform(productions, animals);
    }

    /**
     * Increases the livestock.
     *
     * @param animal   animal type to increase
     * @param quantity quantity to add
     */
    public void increase(Product animal, int quantity) {
        this.animals.add(animal, (float) quantity);
    }

    /**
     * Decreases the livestock.
     *
     * @param animal   animal type to decrease
     * @param quantity quantity to remove
     */
    public void decrease(Product animal, int quantity) {
        this.animals.subtract(animal, (float) quantity);
    }

    public void updateRatio(Ratio ratio) {
        this.ratio = ratio;
    }

    public Ratio getRatio() {
        return ratio;
    }

    public Stocks getAnimals() {
        return animals;
    }
}
