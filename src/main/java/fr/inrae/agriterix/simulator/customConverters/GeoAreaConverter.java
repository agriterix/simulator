package fr.inrae.agriterix.simulator.customConverters;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.ReflectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;
import fr.inrae.agriterix.simulator.GeoArea;

public class GeoAreaConverter extends ReflectionConverter {
    public GeoAreaConverter(Mapper mapper, ReflectionProvider reflectionProvider) {
        super(mapper, reflectionProvider);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        super.marshal(source, writer, context);
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        GeoArea geoArea = (GeoArea) super.unmarshal(reader, context);
        geoArea.initTransient();
        return geoArea;
    }

    @Override
    public boolean canConvert(Class type) {
        return type.equals(GeoArea.class);
    }
}
