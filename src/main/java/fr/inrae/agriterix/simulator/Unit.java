package fr.inrae.agriterix.simulator;

public enum Unit {
    Kg,
    L,
    A; // we consider animals as products here.

    Unit() {
    }
}
