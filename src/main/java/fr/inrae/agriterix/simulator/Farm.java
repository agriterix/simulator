package fr.inrae.agriterix.simulator;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.ListIterator;

public class Farm {

    private final List<Patch> patches;
    private Stocks productions;
    private Livestock livestock;
    private final GeoArea geoArea;
    private Double finances;

    public Farm(GeoArea geoArea) {
        this.geoArea = geoArea;
        this.patches = new ArrayList<>();
        this.productions = new Stocks();
        this.livestock = new Livestock(new HashMap<>(), new HashMap<>());
        this.finances = 0.;
    }

    public int patchesSize() {
        return patches.size();
    }

    /**
     * This method will be called when this farm will be setted to the patch. Please
     * use {@link Patch#setFarm(Farm) }
     *
     * @param patch a Patch
     * @return boolean
     */
    public boolean addPatch(Patch patch) {
        return patches.add(patch);
    }

    public List<Patch> getPatches() {
        return patches;
    }

    public ListIterator<Patch> patchesIterator() {
        return patches.listIterator();
    }

    public Stocks produce() {
        /** produced != productions /!\
         * produced is only what's produced *when* the method is called
         * where productions is the actual stocks.
         */
        Stocks produced = new Stocks();
        for (Patch patch : patches) {
            produced.addStock(patch.produce());
        }
        this.updateProductions();
        productions.addStock(livestock.produce(productions));
        return produced;
    }

    public void updateProductions() {
        for (Patch patch : patches) {
            productions.add(patch.getProduction().getProduct(), patch.getProduction().getValue());
        }
    }

    public Stocks getProductions() {
        return productions;
    }

    public Livestock getLivestock() {
        return this.livestock;
    }

    public void setLivestock(Livestock livestock) {
        this.livestock = livestock;
    }

    public GeoArea getGeoArea() {
        return geoArea;
    }

    public Double getFinances() {
        return finances;
    }

    public void increaseFinances(Double value) {
        finances = finances + value;
    }

    public void decreaseFinances(Double value) {
        finances = finances - value;
    }
}
