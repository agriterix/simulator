package fr.inrae.agriterix.simulator;

import java.util.*;

public class GeoArea {

    private transient List<Patch> patches;
    private List<Flow> flows;
    private String label;

    public GeoArea(List<Flow> flows, List<Patch> patches, String label) {
        this.patches = patches;
        this.flows = flows;
        this.label = label;
    }

    public GeoArea(List<Flow> flows, String label) {
        this.patches = new ArrayList<>();
        this.flows = flows;
        this.label = label;
    }

    public GeoArea(String label) {
        this.patches = new ArrayList<>();
        this.flows = new ArrayList<>();
        this.label = label;
    }

    public GeoArea() {
        initTransient();
    }

    public void initTransient() {
        this.patches = new ArrayList<>();
    }

    private Float getSurface() {
        Float s = 0.f;
        for (Patch p : patches) {
            s += p.getSurface();
        }
        return s;
    }

    public List<Flow> getFlows() {
        return flows;
    }

    public List<Patch> getPatches() {
        return patches;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
