package fr.inrae.agriterix.simulator;

import java.util.Map;

public class Ratio {
    /**
     * Used to specify the ratio of the production, for example 1 Kg of wheat
     * produces 0.75 Kg of flour.
     */
    protected Map<Product, Float> required;
    protected Map<Product, Float> produced;

    public Ratio(Map<Product, Float> required, Map<Product, Float> produced) {
        this.required = required;
        this.produced = produced;
    }

    public Map<Product, Float> getRequired() {
        return required;
    }

    public Map<Product, Float> getProduced() {
        return produced;
    }

    public Stocks transform(Stocks provided, Stocks animals) {
        Stocks transformed = new Stocks();
        boolean remainingA = false;
        boolean remainingP = false;
        // to restore animals stock after production.
        Stocks animalsRemoved = new Stocks();
        do {
            // checking provided and animals
            for (Product key : required.keySet()) {
                if (provided.containsKey(key) && required.containsKey(key)) {
                    remainingP = provided.get(key) >= required.get(key);
                } else if (animals.containsKey(key)) {
                    remainingA = animals.get(key) >= required.get(key);
                }
            }
            // do transformation
            if (remainingA && remainingP) {
                for (Product key : required.keySet()) {
                    // decreasing stock
                    if (provided.containsKey(key)) {
                        provided.set(key, provided.get(key) - required.get(key));
                    } else if (animals.containsKey(key)) {
                        // animals are decreased during the process to estimate the production capacity
                        // but restored after
                        animals.subtract(key, required.get(key));
                        animalsRemoved.add(key, required.get(key));
                    }
                }
                // increasing transformed production
                for (Product key : produced.keySet()) {
                    transformed.add(key, produced.get(key));
                }
            }
        } while (remainingA && remainingP);
        // restoring animals
        //animalsRemoved.forEach((k, v) -> animals.merge(k, v, Float::sum));
        animals.addStock(animalsRemoved);
        return transformed;
    }

    public Stocks transform(Stocks provided) {
        Stocks transformed = new Stocks();
        boolean remaining = false;
        do {
            // checking provided and animals
            for (Product key : required.keySet()) {
                remaining = required.containsKey(key) && provided.containsKey(key)
                        && provided.get(key) >= required.get(key);
            }
            // do transformation
            if (remaining) {
                for (Product key : required.keySet()) {
                    // decreasing stock
                    if (provided.containsKey(key)) {
                        provided.set(key, provided.get(key) - required.get(key));
                    }
                }
                // increasing transformed production
                for (Product key : produced.keySet()) {
                    if (transformed.containsKey(key)) {
                        transformed.set(key, transformed.get(key) + produced.get(key));
                    } else {
                        transformed.set(key, produced.get(key));
                    }
                }
            }
        } while (remaining);
        return transformed;
    }

    @Override
    public String toString() {
        return "{required: " + required + " produced: " + produced + "}";
    }
}
