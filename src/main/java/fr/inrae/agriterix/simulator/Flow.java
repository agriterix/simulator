package fr.inrae.agriterix.simulator;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.*;

public class Flow {

    public static class Input {
        private Product product;
        private Float maximum;

        public Input(Product product, Float maximum) {
            this.product = product;
            this.maximum = maximum;
        }

        public Product getProduct() {
            return product;
        }

        public String getLabel() {
            return product.getLabel();
        }

        public Unit getUnit() {
            return product.getUnit();
        }

        public Float getMaximum() {
            return maximum;
        }

        public void setMaximum(Float maximum) {
            this.maximum = maximum;
        }

        @Override
        public String toString() {
            return "(" + product + ", max:" + maximum + ")";
        }

    }

    private List<Input> inputs;
    /**
     * Remaining stock of the flow;
     */
    private Stocks quantities;
    private List<Product> outputs;
    private Ratio ratio;
    private String label;

    public Flow(List<Input> inputs, Stocks quantities, List<Product> outputType, Ratio ratio) {
        this.inputs = inputs;
        this.quantities = quantities;
        this.outputs = outputType;
        this.ratio = ratio;
        // workaround
        this.label = RandomStringUtils.randomAlphanumeric(10);
    }

    public Flow(List<Input> inputs, Stocks quantities, List<Product> outputType, Ratio ratio,
                String label) {
        this.inputs = inputs;
        this.quantities = quantities;
        this.outputs = outputType;
        this.ratio = ratio;
        this.label = label;
    }

    public int inputsSize() {
        return inputs.size();
    }

    public List<Input> getInputs() {
        return inputs;
    }

    public Ratio getRatio() {
        return ratio;
    }

    public ListIterator<Input> inputsIterator() {
        return inputs.listIterator();
    }


    public Stocks getQuantities() {
        return quantities;
    }

    public int outputsSize() {
        return outputs.size();
    }

    public ListIterator<Product> outputsIterator() {
        return outputs.listIterator();
    }

    public boolean isFull(Product inputType) {
        for (Input input : inputs) {
            if (input.getProduct().equalsTagLess(inputType)) {
                if (quantities.get(inputType) == null) {
                    quantities.empty(inputType);
                    return false;
                } else {
                    return quantities.get(inputType) >= input.maximum;
                }
            } /*else {
                throw new IllegalArgumentException("Input not registered in quantities");
            }*/
        }
        return false;
    }

    @Override
    public String toString() {
        return "{Flow" + this.hashCode() + "\n\tinputs: " + inputs + "\n\toutputs: " + outputs + "\n\tratio: " + ratio
                + "}\n";
    }

    public String getLabel() {
        return label;
    }

    public List<Product> getOutputs() {
        return outputs;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Stocks produce() {
        Stocks produced = new Stocks(this.getRatio().transform(this.getQuantities()));
        this.getQuantities().addStock(produced);
        return produced;
    }

    public double getRemainingStock(Product productType) {
        return quantities.get(productType);
    }
}
