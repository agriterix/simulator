package fr.inrae.agriterix.simulator.dynamics;

import java.util.List;

import fr.inrae.agriterix.simulator.*;

/**
 * Executes the patches agricultural production regarding their constraints and
 * attributes
 */
public class ProductionDynamics extends Dynamics {

    public Stocks produced = new Stocks();

    public ProductionDynamics() {
        initLogger();
    }

    @Override
    public void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, Flow global,
                        Parameters parameters, TimeUnit timeUnit) {
        for (Farmer farmer : farmers) {
            produced.addStock(farmer.getFarm().produce());
            logger.logQuantities(context.getTimestep(), farmer.getFarm().getGeoArea(), produced,
                    "localProduction");
        }
        produced.clear();
        for (GeoArea geoArea : geoAreas) {
            for (Flow flow : geoArea.getFlows()) {
                produced.addStock(flow.produce());
            }
            logger.logQuantities(context.getTimestep(), geoArea, produced, "localProduction");
        }
        produced.clear();
    }

    @Override
    public void initLogger() {
        logger = new DynamicsLogger(this.getClass().getSimpleName(), super.timeStamp, "TIMESTAMP, GEOAREA, PRODUCT, " +
                "AMOUNT, LABEL");
    }

    @Override
    public void initParameters() {

    }
}
