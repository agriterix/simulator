package fr.inrae.agriterix.simulator.dynamics;

import fr.inrae.agriterix.simulator.Flow;
import fr.inrae.agriterix.simulator.GeoArea;
import fr.inrae.agriterix.simulator.Product;
import fr.inrae.agriterix.simulator.Stocks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class DynamicsLogger {

        private FileHandler fileHandler;
        private Logger logger;
        private String separator = ",";
        private String path = "data/out";

        public DynamicsLogger(String className, String timeStamp, String... headers) {
            try {
                Files.createDirectories(Paths.get("data/out/" + timeStamp + "/logs/"));
                fileHandler = new FileHandler("data/out/" + timeStamp + "/logs/" + className + ".csv", false);
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger = Logger.getLogger(className);
            logger.setUseParentHandlers(false);
            logger.addHandler(fileHandler);
            fileHandler.setFormatter(new DynamicsLogFormatter(String.join(separator, headers)));
        }


        public void setHeader(String header) {
            logger.info(header);
        }

        public void log(int timestep, String message) {
            logger.info(timestep + separator + message);
        }

        public void log(int timestep, Object... objects) {
            StringBuilder buffer = new StringBuilder();
            buffer.append(timestep);
            for (Object object : objects) {
                buffer.append(separator);
                buffer.append(object);
            }
            logger.info(buffer.toString());
        }

        public void logQuantities(int timestep, GeoArea geoArea, Stocks quantities, String label) {
            for (Map.Entry<Product, Float> quantity: quantities.entrySet()) {
                logger.info(timestep + separator + geoArea.getLabel() + separator + quantity.getKey() + separator
                        + quantity.getValue() + separator + label);
            }
        }

        public void logEvolutions(int timestep, Flow flow, String category, Product product, Float value) {
                logger.info(timestep + separator + category + separator + flow.getLabel() + separator + product
                        + separator + value);
        }
}
