package fr.inrae.agriterix.simulator.dynamics;


import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import fr.inrae.agriterix.simulator.*;
import fr.inrae.agriterix.simulator.Flow.Input;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MarketDynamics extends Dynamics {

    private float exportFactor;
    private float importFactor;
    private Stocks localExchanged = new Stocks();
    private Stocks importation = new Stocks();
    private Stocks exportation = new Stocks();
    private List<String[]> scenario;
    private int counter;


    public MarketDynamics() {
        initLogger();
    }

    @Override
    public void initLogger() {
        logger = new DynamicsLogger(this.getClass().getSimpleName(), super.timeStamp, "TIMESTAMP, GEOAREA, PRODUCT, " +
                "AMOUNT, LABEL");
    }

    @Override
    public void initParameters() {
        try (CSVReader reader = new CSVReader(new FileReader(configPath + "import_export.csv"))) {
            scenario = reader.readAll();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CsvException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, Flow global,
                        Parameters parameters, TimeUnit timeUnit) {
        // default values
        exportFactor = 1;
        importFactor = 1;

        for (GeoArea geoArea : geoAreas) {
            // selecting import/export factors from scenario
            for (int i = counter; i < scenario.size(); i++) {
                if (Integer.parseInt(scenario.get(i)[0]) == context.getTimestep()) {
                    if (scenario.get(i)[1] == geoArea.getLabel()) {
                        importFactor = Float.parseFloat(scenario.get(i)[2]);
                        exportFactor = Float.parseFloat(scenario.get(i)[3]);
                        assert importFactor <= 1;
                        assert exportFactor <= 1;
                        counter++;
                        break;
                    }
                }
            }

            // tagged farm stock
            for (Farmer farmer : farmers) {
                matchingTagAllocationProduction(farmer.getFarm().getProductions(), geoArea.getFlows());
                globalExport(farmer.getFarm().getProductions(), global, exportFactor);
            }
            // tagged flow stock
            for (Flow flow : geoArea.getFlows()) {
                matchingTagAllocationProduction(flow.getQuantities(), geoArea.getFlows().stream()
                        .filter(e -> !geoArea.getFlows().contains(flow))
                        .collect(Collectors.toList()));
                globalImport(flow, global, importFactor);
                globalExport(flow.getQuantities(), global, exportFactor);
            }
            logger.logQuantities(context.getTimestep(), geoArea, localExchanged, "local");
            logger.logQuantities(context.getTimestep(), geoArea, importation, "import");
            logger.logQuantities(context.getTimestep(), geoArea, exportation, "export");
            // reset
            exportation.clear();
            importation.clear();
            localExchanged.clear();
        }
    }

    public void globalImport(Flow flow, Flow global, Float importFactor) {
        for (Map.Entry<Product, Float> production : global.getQuantities().entrySet()) {
            for (Input input : flow.getInputs()) {
                if (input.getProduct().equals(production.getKey())) {
                    if (input.getProduct().getTags().equals(production.getKey().getTags())) {
                        allocate(production, input, flow, importation, importFactor);
                    }
                }
            }
        }
    }

    public void globalExport(Stocks stocks, Flow global, Float exportFactor) {
        for (Map.Entry<Product, Float> production : stocks.entrySet()) {
            for (Input input : global.getInputs()) {
                if (input.getProduct().equals(production.getKey()))
                    if (input.getProduct().getTags().equals(production)) {
                        allocate(production, input, global, exportation, exportFactor);
                    }
            }
        }
    }

    /**
     * Production allocation based on tag matching.
     *
     * @param productions map of product, quantity
     * @param flows       list of flows
     */
    public void matchingTagAllocationProduction(Stocks productions, List<Flow> flows) {
        // for each product check if each flow has input w/ all tags and allocate
        for (Map.Entry<Product, Float> production : productions.entrySet()) {
            for (Flow flow : flows) {
                for (Input input : flow.getInputs()) {
                    // products match
                    if (input.getProduct().equals(production.getKey())) {
                        // check all tags
                        if (input.getProduct().getTags().equals(production.getKey().getTags())) {
                            allocate(production, input, flow, localExchanged, 1f);
                        }
                    }
                }
            }
        }
        // for each product check if each flow has input w/ at least one tag (pick max) and allocate
        for (Map.Entry<Product, Float> production : productions.entrySet()) {
            for (Flow flow : flows) {
                for (Input input : flow.getInputs()) {
                    // products match
                    if (input.getProduct().equals(production.getKey())) {
                        // check for one tag matching
                        for (Tag tag : input.getProduct().getTags()) {
                            if (production.getKey().getTags().contains(tag)) {
                                allocate(production, input, flow, localExchanged, 1f);
                            }
                        }
                    }
                }
            }
        }
    }


    public void allocate(Map.Entry<Product, Float> production, Input input, Flow flow, Stocks stocksLog, Float factor) {
        if (!flow.isFull(production.getKey())) {
            if (production.getValue() <= input.getMaximum()) {
                // all is transferred
                flow.getQuantities().add(production.getKey(), production.getValue() * factor);
                // logging it
                addToLog(production.getKey(), production.getValue() * factor, stocksLog);
                // updating production
                production.setValue(0.f + production.getValue() * (1 - factor));
            } else {
                // partially transferred
                flow.getQuantities().add(production.getKey(), input.getMaximum() * factor);
                // logging it
                addToLog(production.getKey(), input.getMaximum() * factor, stocksLog);
                // updating production
                production.setValue(production.getValue() - (input.getMaximum() * factor));
            }
        }
    }

    public void addToLog(Product product, Float value, Stocks stocksLog) {
        stocksLog.add(product, value);
    }
}