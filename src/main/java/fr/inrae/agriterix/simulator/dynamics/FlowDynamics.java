package fr.inrae.agriterix.simulator.dynamics;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import fr.inrae.agriterix.simulator.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlowDynamics extends Dynamics {

    private List<String[]> scenario;
    private int counter;

    public FlowDynamics() {

    }

    @Override
    public void initLogger() {
        logger = new DynamicsLogger(this.getClass().getSimpleName(), super.timeStamp, "TIMESTAMP, CATEGORY, FLOW_LABEL, " +
                "PRODUCT, VALUE");
    }

    @Override
    public void initParameters() {
        try (CSVReader reader = new CSVReader(new FileReader(configPath + "evolutions.csv"))) {
            scenario = reader.readAll();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CsvException e) {
            e.printStackTrace();
        }
        counter = 0;
    }

    @Override
    public void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, Flow global,
                        Parameters parameters, TimeUnit timeUnit) {

        Map<String, Float> inputsMod = new HashMap<>();
        Map<String, Float> requiredMod = new HashMap<>();
        Map<String, Float> producedMod = new HashMap<>();

        for (int i = counter; i < scenario.size(); i++) {
            // checking timestep
            if (Integer.parseInt(scenario.get(counter)[0]) == context.getTimestep()) {
                for (GeoArea geoArea : geoAreas) {
                    // checking geoArea
                    if (scenario.get(i)[4] == geoArea.getLabel()) {
                        // checking flowtype
                        if (scenario.get(i)[3].equals("Inputs")) {
                            inputsMod.put(scenario.get(i)[1], Float.parseFloat(scenario.get(i)[2]));
                        } else if (scenario.get(i)[3].equals("Required")) {
                            requiredMod.put(scenario.get(i)[1], Float.parseFloat(scenario.get(i)[2]));
                        } else if (scenario.get(i)[3].equals("Produced")) {
                            producedMod.put(scenario.get(i)[1], Float.parseFloat(scenario.get(i)[2]));
                        }
                    }
                    for (Flow flow : geoArea.getFlows()) {
                        editInputs(context.getTimestep(), flow, inputsMod);
                        editRequirements(context.getTimestep(), flow, requiredMod);
                        editProduced(context.getTimestep(), flow, producedMod);
                    }
                }
                counter++;
            } else {
                break;
            }
        }
    }

    /**
     * Changes the maximum value of inputs
     *
     * @param timestep current timestep
     * @param flow     selected flow
     * @param mods     product label → factor map
     */
    public void editInputs(int timestep, Flow flow, Map<String, Float> mods) {
        for (Flow.Input input : flow.getInputs()) {
            for (Map.Entry<String, Float> entry : mods.entrySet()) {
                if (input.getProduct().getLabel().equals(entry.getKey())) {
                    input.setMaximum(input.getMaximum() * entry.getValue());
                    logger.logEvolutions(timestep, flow, "input", input.getProduct(), entry.getValue());
                }
            }
        }
    }

    /**
     * Changes the value of requirements
     *
     * @param timestep current timestep
     * @param flow     selected flow
     * @param mods     product label → factor map
     */
    public void editRequirements(int timestep, Flow flow, Map<String, Float> mods) {
        for (Product required : flow.getRatio().getRequired().keySet()) {
            for (Map.Entry<String, Float> entry : mods.entrySet()) {
                if (required.getLabel().equals(entry.getKey())) {
                    flow.getRatio().getRequired().put(required, flow.getRatio().getRequired()
                            .get(required) * entry.getValue());
                    logger.logEvolutions(timestep, flow, "requirement", required, entry.getValue());
                }
            }
        }
    }

    /**
     * Changes the value of produced
     *
     * @param timestep current timestep
     * @param flow     selected flow
     * @param mods     product label → factor map
     */
    public void editProduced(int timestep, Flow flow, Map<String, Float> mods) {
        for (Product produced : flow.getRatio().getProduced().keySet()) {
            for (Map.Entry<String, Float> entry : mods.entrySet()) {
                if (produced.getLabel().equals(entry.getKey())) {
                    flow.getRatio().getProduced().put(produced, flow.getRatio().getProduced()
                            .get(produced) * entry.getValue());
                    logger.logEvolutions(timestep, flow, "produced", produced, entry.getValue());
                }
            }
        }
    }

}
