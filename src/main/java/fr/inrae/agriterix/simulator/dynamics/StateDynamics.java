package fr.inrae.agriterix.simulator.dynamics;

import fr.inrae.agriterix.simulator.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Here we can log global state of the simulator at each timestep, i.e. Farmers numbers, global products stocks etc.
 */
public class StateDynamics extends Dynamics {

    DynamicsLogger ageClassLogger;

    @Override
    public void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, Flow global,
                        Parameters parameters, TimeUnit timeUnit) throws IOException {
        int[] ageClasses = new int[82];
        // all geoAreas
        for (Farmer farmer : farmers) {
            ageClasses[farmer.getAge() - 18] += 1;
        }
        ageClassLogger.log(context.getTimestep(), Arrays.toString(ageClasses).replace("[", "")
                .replace("]", ""));
    }

    @Override
    public void initLogger() {
        //TODO multiple loggers that plots age distributions
        int[] ageClassesHeader = new int[82];
        for (int i = 0; i < ageClassesHeader.length; i++) {
            ageClassesHeader[i] = i + 18;
        }
        ageClassLogger = new DynamicsLogger("AgeClasses", super.timeStamp, "timestamp",
                Arrays.toString(ageClassesHeader).replace("[", "")
                        .replace("]", ""));
    }

    @Override
    public void initParameters() {

    }
}
