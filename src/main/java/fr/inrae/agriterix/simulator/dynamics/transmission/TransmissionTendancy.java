package fr.inrae.agriterix.simulator.dynamics.transmission;

import fr.inrae.agriterix.simulator.Farmer;
import fr.inrae.agriterix.simulator.GeoArea;
import fr.inrae.agriterix.simulator.Patch;
import fr.inrae.agriterix.simulator.Product;
import fr.inrae.agriterix.simulator.SimulationContext;
import fr.inrae.agriterix.simulator.Unit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This transmission implementation try to reproduce the current tendancy.
 */
public class TransmissionTendancy implements TransmissionStrategy {

    // Is the transmission realized with only one successor?
    // TODO better with a distribution on number of successors
    private double singleSuccessorProba;
    private int maxSuccessors;
    // Is the transmission realized with an existant farmer (instead of a new)
    private double existantSuccessorProba;
    private Map<String, Map<String, Double>> transitionMatrix;

    public TransmissionTendancy() {
    }

    public TransmissionTendancy(double singleSuccessorProba, double existantSuccessorProba, int maxSuccessors, Map<String, Map<String, Double>> transitionMatrix) {
        this.singleSuccessorProba = singleSuccessorProba;
        this.existantSuccessorProba = existantSuccessorProba;
        this.maxSuccessors = maxSuccessors;
        this.transitionMatrix = transitionMatrix;
    }

    @Override
    public void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, List<Integer> installationsAges, int[] retirements) {
        for (int i = 0; i < retirements.length; i++) {
            int nbRetired = retirements[i];
            final int age = i;
            List<Farmer> ageIFarmers = farmers.stream().filter(f -> f.getAge() == age).collect(Collectors.toList());
            for (int j = 0; j < nbRetired; j++) {
                final Farmer retiredFarmer = context.nextObject(ageIFarmers, true);
                farmers.remove(retiredFarmer);
                if (context.nextDouble() <= singleSuccessorProba) {
                    // single successor
                    processSuccessor(installationsAges, context, farmers, retiredFarmer);
                } else { // multiple successors
                    List<Farmer> successors = new ArrayList<>();
                    for (int k = 0; k < context.nextInt(2, maxSuccessors); k++) {
                        processSuccessor(installationsAges, context, farmers, retiredFarmer); // new farmer
                    }
                }
            }
        }
    }

    private void processSuccessor(List<Integer> installationsAges, SimulationContext context, List<Farmer> farmers, final Farmer retiredFarmer) {
        if (installationsAges.isEmpty() || context.nextDouble() <= existantSuccessorProba) {
            // existant farmer
            Farmer successor = context.nextObject(farmers, false);
            patchTransition(retiredFarmer, successor, true, context);
        } else {
            // new farmer
            Farmer newFarmer = new Farmer(context.nextObject(installationsAges, true), retiredFarmer.getFarm(), context.setIdFarmer());
            patchTransition(retiredFarmer, newFarmer, false, context);
        }
    }

    private void patchTransition(final Farmer retiredFarmer, Farmer successor, boolean isExistantFarmer, SimulationContext context) {
        for (Patch patch : retiredFarmer.getFarm().getPatches()) {
            if (isExistantFarmer) {
                patch.setFarm(successor.getFarm());
            }
            String product = context.nextMapObjectWithDistributionInKeysBis(
                    transitionMatrix.get(patch.getProduction().getProduct().getLabel())
            );
            if (!product.equals(patch.getProduction().getProduct().getLabel())) {
                // TODO correctly fetch existing product or good initialization
                patch.setProduction(new Patch.Production(new Product(product, Unit.Kg), 0.f));
            }
        }
    }

}
