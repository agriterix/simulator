package fr.inrae.agriterix.simulator.dynamics;

import fr.inrae.agriterix.simulator.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public abstract class Dynamics {

    protected transient DynamicsLogger logger;
    protected String timeStamp = "timestamp";
    protected String configPath;

    public abstract void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, Flow global,
                                 Parameters parameters, TimeUnit timeUnit) throws IOException;

    public abstract void initLogger();

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }

    public abstract void initParameters();
}
