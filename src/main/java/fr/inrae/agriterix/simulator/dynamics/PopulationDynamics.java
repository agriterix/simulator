package fr.inrae.agriterix.simulator.dynamics;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.NoTypePermission;
import fr.inrae.agriterix.simulator.*;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.RandomType;
import net.andreinc.mockneat.unit.objects.Probabilities;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Deprecated
public class PopulationDynamics extends Dynamics {

    // incoming simulation parameters, may be replaced by complex functions
    private int RETIREMENT_AGE;
    private int MIN_AGE_TAKEOVER;
    private int MAX_AGE_TAKEOVER;
    private int NEWCOMERS;
    private double TAKEOVER_CHANCE;
    private double YOUNG_DEPARTURE_CHANCE;
    private double OLD_DEPARTURE_CHANCE;
    private double P;
    private double PATCH_CHANGE_CHANCE;

    private String transitionFile;

    //TODO REPLACE by transition matrix (cultural practice is determined by product tag ?)
    private final CulturalPractice transitionPractice = CulturalPractice.O;
    private Product transitionProduct = new Product("Soja", Unit.Kg);


    private Map<String, Map<String, Double>> transitionMatrix;

    public PopulationDynamics() {
    }

    @SuppressWarnings("unchecked")
    public void initParameters() {
        transitionFile = "transitions.xml";
        initLogger();
        Yaml yaml = new Yaml();
        try {
            InputStream inputStream = new FileInputStream(configPath + "population.yaml");
            Map<String, Object> config = yaml.load(inputStream);
            RETIREMENT_AGE = (int) config.get("retirement_age");
            MIN_AGE_TAKEOVER = (int) config.get("min_age_takeover");
            MAX_AGE_TAKEOVER = (int) config.get("max_age_takeover");
            NEWCOMERS = (int) config.get("newcomers");
            TAKEOVER_CHANCE = (double) config.get("takeover_chance");
            YOUNG_DEPARTURE_CHANCE = (double) config.get("young_departure_chance");
            OLD_DEPARTURE_CHANCE = (double) config.get("old_departure_chance");
            P = (double) config.get("p");
            PATCH_CHANGE_CHANCE = (double) config.get("patch_change_chance");
            XStream xStream = new XStream();
            XStream.setupDefaultSecurity(xStream);
            xStream.allowTypesByRegExp(new String[]{".*"});
            FileInputStream fileInputStream = new FileInputStream(configPath + transitionFile);
            transitionMatrix = (Map<String, Map<String, Double>>) xStream.fromXML(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, Flow global,
                        Parameters parameters, TimeUnit timeUnit) {

        List<Farmer> farmersToRetire = new ArrayList<>();
        List<Farmer> farmersToInstall = new ArrayList<>();
        // retirement
        for (Farmer current : farmers) {
            current.aging();
            // old
            if (current.getAge() >= RETIREMENT_AGE) {
                if (context.nextDouble() <= OLD_DEPARTURE_CHANCE) {
                    // installation
                    if (context.nextDouble() <= TAKEOVER_CHANCE) {
                        farmersToInstall.addAll(acquisitions(context, current, current.getFarm().getGeoArea(), farmers));
                    }
                    farmersToRetire.add(current);
                }
            }
            // young
            else {
                if (context.nextDouble() <= YOUNG_DEPARTURE_CHANCE) {
                    // installation
                    if (context.nextDouble() <= TAKEOVER_CHANCE) {
                        farmersToInstall.addAll(acquisitions(context, current, current.getFarm().getGeoArea(), farmers));
                    }
                    farmersToRetire.add(current);
                }
            }
        }
        for (Farmer farmer : farmersToRetire) {
            farmers.remove(farmer);
        }
        farmers.addAll(farmersToInstall);
    }

    @Override
    public void initLogger() {
        logger = new DynamicsLogger(this.getClass().getSimpleName(), super.timeStamp, "HEADER");
    }

    /**
     * https://forgemia.inra.fr/agriterix/doc/-/blob/master/acquisition_scenarios.md
     */
    public List<Farmer> acquisitions(SimulationContext context, Farmer current, GeoArea geoArea, List<Farmer> farmers) {
        // TODO split in minimal subfunctions (createNew, createNews, getCurrent, getCurrents, changePatches, etc.)
        List<Farmer> newcomers = new ArrayList<>();
        // Single new identical
        if (context.nextDouble() <= P) {
            newcomers.add(singleNewIdentical(context, current.getFarm()));
        }
        // Single new different
        else if (context.nextDouble() > P && context.nextDouble() <= 2 * P) {
            newcomers.add(singleNewDifferent(context, current.getFarm()));
        }
        // Multiple new identical
        else if (context.nextDouble() > 2 * P && context.nextDouble() <= 3 * P) {
            newcomers.addAll(multipleNewIdentical(context, current.getFarm(), geoArea));
        }
        // Multiple new different
        else if (context.nextDouble() > 3 * P && context.nextDouble() <= 4 * P) {
            newcomers.addAll(multipleNewDifferent(context, current.getFarm(), geoArea));
        }
        // Single current identical
        else if (context.nextDouble() > 4 * P && context.nextDouble() <= 5 * P) {
            singleCurrentIdentical(context, current, farmers);
        }
        // Single current different
        else if (context.nextDouble() > 5 * P && context.nextDouble() <= 6 * P) {
            singleCurrentDifferent(context, current, farmers);
        }
        // Multiple current identical
        else if (context.nextDouble() > 6 * P && context.nextDouble() <= 7 * P) {
            multipleCurrentIdentical(context, current, farmers);
        }
        // Multiple current different
        else if (context.nextDouble() > 7 * P) {
            multipleCurrentDifferent(context, current, farmers);

        }
        return newcomers;
    }

    public Farmer singleNewIdentical(SimulationContext context, Farm farm) {
        return new Farmer(context.nextInt(MIN_AGE_TAKEOVER, MAX_AGE_TAKEOVER), farm, context.setIdFarmer());
    }

    public Farmer singleNewDifferent(SimulationContext context, Farm farm) {
        Farmer newcomer = new Farmer(context.nextInt(MIN_AGE_TAKEOVER, MAX_AGE_TAKEOVER), farm, context.setIdFarmer());
        List<Patch> patchesChanged = new ArrayList<>();
        List<Patch> patchesToRemove = new ArrayList<>();
        for (Patch patch : newcomer.getFarm().getPatches()) {
            if (context.nextDouble() >= PATCH_CHANGE_CHANCE) {
                if (transitionMatrix.get(patch.getProduction()
                        .getProduct().getLabel()) == null) {
                    System.out.println(transitionMatrix);
                    System.out.println(patch.getProduction().getProduct());
                }
                transitionProduct.setLabel(context.nextMapObjectWithDistributionInKeysBis(transitionMatrix
                        .get(patch.getProduction().getProduct().getLabel())));
                patchesChanged.add(new Patch(patch.getGeoArea(), patch.getSurface(), transitionPractice,
                        transitionProduct, patch.getId()));
                patchesToRemove.add(patch);
            }
        }
        for (Patch changed : patchesChanged) {
            newcomer.getFarm().addPatch(changed);
        }
        for (Patch toRemove : patchesToRemove) {
            newcomer.getFarm().getPatches().remove(toRemove);
        }
        return newcomer;
    }

    public List<Farmer> multipleNewIdentical(SimulationContext context, Farm farm, GeoArea geoArea) {
        List<Farmer> newcomers = new ArrayList<>();
        int start = 0;
        int size = farm.getPatches().size();
        int end = size / NEWCOMERS;
        // dirty splitting
        for (int i = 0; i < NEWCOMERS; i++) {
            List<Patch> split = new ArrayList<>();
            if (end < size) {
                split = new ArrayList<>(farm.getPatches().subList(start, end));
            } else if (size > 0) {
                split = new ArrayList<>(farm.getPatches().subList(size - i, size));
            }
            Farm created = new Farm(geoArea);
            for (Patch patch : split) {
                created.addPatch(patch);
            }
            newcomers.add(singleNewIdentical(context, created));
            start = start + end;
            end = end + end;
        }
        return newcomers;
    }

    public List<Farmer> multipleNewDifferent(SimulationContext context, Farm farm, GeoArea geoArea) {
        List<Farmer> newcomers = new ArrayList<>();
        int start = 0;
        int size = farm.getPatches().size();
        int end = size / NEWCOMERS;
        // dirty splitting
        for (int i = 0; i < NEWCOMERS; i++) {
            List<Patch> split = new ArrayList<>();
            if (end < size) {
                split = new ArrayList<>(farm.getPatches().subList(start, end));
            } else if (size > 0) {
                split = new ArrayList<>(farm.getPatches().subList(size - i, size));
            }
            Farm created = new Farm(geoArea);
            for (Patch patch : split) {
                created.addPatch(patch);
            }
            newcomers.add(singleNewDifferent(context, created));
            start = start + end;
            end = end + end;
        }
        return newcomers;
    }

    public void singleCurrentIdentical(SimulationContext context, Farmer current, List<Farmer> farmers) {
        Farmer retaker = farmers.get(context.nextInt(0, farmers.size() - 1));
        List<Patch> patches = new ArrayList<>(current.getFarm().getPatches());
        for (Patch patch : patches) {
            retaker.getFarm().addPatch(patch);
        }
    }

    public void singleCurrentDifferent(SimulationContext context, Farmer current, List<Farmer> farmers) {
        Farmer retaker = farmers.get(context.nextInt(0, farmers.size() - 1));
        List<Patch> patchesChanged = new ArrayList<>();
        List<Patch> patchesToRemove = new ArrayList<>();
        for (Patch patch : current.getFarm().getPatches()) {
            if (context.nextDouble() >= PATCH_CHANGE_CHANCE) {
                transitionProduct.setLabel(context.nextMapObjectWithDistributionInKeysBis(transitionMatrix
                        .get(patch.getProduction().getProduct().getLabel())));
                patchesChanged.add(new Patch(patch.getGeoArea(), patch.getSurface(), transitionPractice,
                        transitionProduct, patch.getId()));
                patchesToRemove.add(patch);
            }
        }
        for (Patch changed : patchesChanged) {
            current.getFarm().addPatch(changed);
        }
        for (Patch toRemove : patchesToRemove) {
            current.getFarm().getPatches().remove(toRemove);
        }
        List<Patch> patches = new ArrayList<>(current.getFarm().getPatches());
        for (Patch patch : patches) {
            retaker.getFarm().addPatch(patch);
        }
    }

    public void multipleCurrentIdentical(SimulationContext context, Farmer current, List<Farmer> farmers) {
        int start = 0;
        int size = current.getFarm().getPatches().size();
        int end = size / NEWCOMERS;
        // dirty splitting
        for (int i = 0; i < NEWCOMERS; i++) {
            Farmer retaker = farmers.get(context.nextInt(0, farmers.size() - 1));
            List<Patch> split = new ArrayList<>();
            if (end < size) {
                split = new ArrayList<>(current.getFarm().getPatches().subList(start, end));
            } else if (size > 0) {
                split = new ArrayList<>(current.getFarm().getPatches().subList(size - i, size));
            }
            for (Patch patch : split) {
                retaker.getFarm().addPatch(patch);
            }
            start = start + end;
            end = end + end;
        }
    }

    public void multipleCurrentDifferent(SimulationContext context, Farmer current, List<Farmer> farmers) {
        int start = 0;
        int size = current.getFarm().getPatches().size();
        int end = size / NEWCOMERS;
        // dirty splitting
        for (int i = 0; i < NEWCOMERS; i++) {
            Farmer retaker;
            if (farmers.size() > 1) {
                retaker = farmers.get(context.nextInt(0, farmers.size() - 1));
            } else {
                retaker = farmers.get(0);
            }
            List<Patch> split = new ArrayList<>();
            if (end < size) {
                split = new ArrayList<>(current.getFarm().getPatches().subList(start, end));
            } else if (size > 0) {
                split = new ArrayList<>(current.getFarm().getPatches().subList(size - i, size));
            }
            for (Patch patch : split) {
                retaker.getFarm().addPatch(patch);
            }
            start = start + end;
            end = end + end;
        }
    }
}
