package fr.inrae.agriterix.simulator.dynamics;

import fr.inrae.agriterix.simulator.*;
import fr.inrae.agriterix.simulator.dynamics.transmission.TransmissionStrategy;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import umontreal.ssj.probdist.BetaDist;

public class PopulationDynamicsByAgeClass extends Dynamics {

    private double amplitude, drop_x, mu, v, r_threshold, r_factor;
    private TransmissionStrategy transmission;

    public PopulationDynamicsByAgeClass() {
    }

    public PopulationDynamicsByAgeClass(double amplitude, double drop_x, double mu, double v, double r_threshold,
                                        double r_factor, TransmissionStrategy transmission) {
        this.amplitude = amplitude;
        this.drop_x = drop_x;
        this.mu = mu;
        this.v = v;
        this.r_threshold = r_threshold;
        this.r_factor = r_factor;
        this.transmission = transmission;
    }
    
    @Override
    public void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, Flow global,
                        Parameters parameters, TimeUnit timeUnit) {
        int[] ageclasses = new int[120];
        Arrays.fill(ageclasses, 0);
        for (Farmer farmer : farmers) {
            ageclasses[farmer.getAge()] += 1;
        }
        int popSum = IntStream.of(ageclasses).sum();
        double alpha = mu * v;
        double beta = (1 - mu) * v;
        int[] installations = IntStream.range(0, ageclasses.length).map(
                idx -> (int) Math.round(ageclasses[idx] * popSum * amplitude
                        * BetaDist.density(alpha, beta, idx / drop_x))
            ).toArray();
        List<Integer> installationsAges = new ArrayList<>();
        for (int i = 0; i < installations.length; i++) {
            for (int j = 0; j < installations[i]; j++) {
                installationsAges.add(getAge(i));
            }
            
        }
        int[] retirements = IntStream.range(0, ageclasses.length).map(
                idx -> idx>=r_threshold ? (int)Math.round(r_factor * ageclasses[idx]) : 0
        ).toArray();
        transmission.process(context, geoAreas, farmers, installationsAges, retirements);
        logger.log(context.getTimestep(), farmers.size(), Arrays.stream(installations).sum(), Arrays.stream(retirements).sum());
     }

    private int getAge(int ageclass) {
        return ageclass + 18;
    }
    
    @Override
    public void initLogger() {
        logger = new DynamicsLogger(this.getClass().getSimpleName(), super.timeStamp, "Timestep", "nb farmers","nb installations", "nb retirements");
    }

    @Override
    public void initParameters() {

    }

}
