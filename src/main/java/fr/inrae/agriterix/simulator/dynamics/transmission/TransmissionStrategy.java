package fr.inrae.agriterix.simulator.dynamics.transmission;

import fr.inrae.agriterix.simulator.Farmer;
import fr.inrae.agriterix.simulator.GeoArea;
import fr.inrae.agriterix.simulator.SimulationContext;
import java.util.List;

public interface TransmissionStrategy {

    public void process(SimulationContext context, List<GeoArea> geoAreas, List<Farmer> farmers, List<Integer> installationsAges, int[] retirements);
}
