package fr.inrae.agriterix.simulator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import co.nstant.in.cbor.CborBuilder;
import co.nstant.in.cbor.CborEncoder;
import co.nstant.in.cbor.CborException;
import co.nstant.in.cbor.builder.ArrayBuilder;
import com.thoughtworks.xstream.XStream;

import fr.inrae.agriterix.simulator.customConverters.GeoAreaConverter;
import fr.inrae.agriterix.simulator.dynamics.*;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.mutable.MutableInt;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.opengis.feature.simple.SimpleFeature;

public class Simulator {

    private final List<GeoArea> geoAreas;
    private final Flow global;
    private final List<Farmer> farmers;
    private transient List<Dynamics> dynamicsList;
    private String timestamp;
    private transient Parameters parameters;
    private transient SimulationContext context;
    private transient MutableInt timestep;
    private transient XStream xstream;
    private String paramsPath;

    public Simulator(Parameters parameters, List<Farmer> farmers) {
        this.parameters = parameters;
        this.geoAreas = new ArrayList<>();
        for (Farmer farmer : farmers) {
            for (Patch patch : farmer.getFarm().getPatches()) {
                if (!this.geoAreas.contains(patch.getGeoArea())) {
                    this.geoAreas.add(patch.getGeoArea());
                }
            }
        }
        this.farmers = farmers;
        this.timestamp = java.time.LocalDateTime.now().toString();
        this.dynamicsList = parameters.getDynamics();
        this.initIgnoredFields();
        for (Dynamics dynamics : dynamicsList) {
            dynamics.setTimeStamp(timestamp);
        }
        this.global = createGlobal();
    }

    // quick & dirty method to create a global flow aimed to be replaced by a parameterized version by the generator.
    public Flow createGlobal() {
        Set<Product> productsIn = new HashSet<>();
        Set<Product> productsOut = new HashSet<>();
        for (GeoArea geoArea : geoAreas) {
            if (geoArea.getFlows() != null) {
                for (Flow flow : geoArea.getFlows()) {
                    for (Flow.Input input : flow.getInputs()) {
                        productsIn.add(input.getProduct());
                    }
                    for (Product output : flow.getOutputs()) {
                        productsOut.add(output);
                    }
                }
                for (Patch patch : geoArea.getPatches()) {
                    productsOut.add(patch.getProduction().getProduct());
                }
                if (farmers != null) {
                    for (Farmer farmer : farmers) {
                        productsOut.addAll(farmer.getFarm().getLivestock().getRatio().getProduced().keySet());
                    }
                }
            }
        }
        List<Flow.Input> inputs = new ArrayList<>();
        List<Product> outputs = new ArrayList<>();
        Stocks quantities = new Stocks();
        for (Product product : productsIn) {
            inputs.add(new Flow.Input(product, Float.MAX_VALUE));
            // quantities.add(product, Float.MAX_VALUE);
        }
        for (Product product : productsOut) {
            outputs.add(product);
            quantities.add(product, Float.MAX_VALUE);
        }
        Map<Product, Float> required = new ConcurrentHashMap<>();
        Map<Product, Float> produced = new ConcurrentHashMap<>();
        Ratio ratio = new Ratio(required, produced);
        return new Flow(inputs, quantities, outputs, ratio, "Global");
    }

    /**
     * Create the xstream object for writing/reading configuration files. This
     * method is called by the constructor, and you only need to call this method
     * if you want to use the XStream configuration without construct a Simulator
     * instance.
     *
     * @return xstream
     */
    public static XStream createXstream() {
        XStream xstream = new XStream();
        XStream.setupDefaultSecurity(xstream);
        xstream.allowTypesByRegExp(new String[]{".*"});
        xstream.registerConverter(new GeoAreaConverter(xstream.getMapper(), xstream.getReflectionProvider()));
        // not useful
        //xstream.addImplicitCollection(Product.class, "tags");
        //xstream.processAnnotations(Product.class);
        xstream.addImplicitCollection(GeoArea.class, "flows");
        String namespace = "";
        xstream.omitField(MarketDynamics.class, "logger");
        xstream.useAttributeFor(GeoArea.class, "label");
        xstream.useAttributeFor(Product.class, "label");
        xstream.useAttributeFor(Product.class, "unit");
        xstream.useAttributeFor(Flow.Input.class, "maximum");
        xstream.useAttributeFor(Patch.class, "surface");
        xstream.useAttributeFor(Patch.class, "practice");
        xstream.aliasPackage("simulator", "fr.inrae.agriterix.simulator");
        xstream.alias(namespace + "Input", Flow.Input.class);
        for (Class type : new Class[]{Product.class, Patch.class, GeoArea.class, Flow.class, Livestock.class,
                Ratio.class, Tag.class, Farmer.class, Farm.class, Unit.class, CulturalPractice.class, Parameters.class,
                Simulator.class, Product.class, MarketDynamics.class, ProductionDynamics.class,
                PopulationDynamicsByAgeClass.class, FlowDynamics.class, Dynamics.class}) {
            xstream.alias(namespace + type.getSimpleName(), type);
        }
        // useful w/ xpath
        xstream.setMode(XStream.SINGLE_NODE_XPATH_RELATIVE_REFERENCES);
        // otherwise w/ ids :
        // xstream.setMode(XStream.ID_REFERENCES);
        return xstream;
    }


    private void initIgnoredFields() {
        this.timestamp = java.time.LocalDateTime.now().toString();
        this.timestep = new MutableInt(0);
        this.context = new SimulationContext(timestep, parameters.getRngSeedIndex());
        this.xstream = createXstream();
        for (Farmer farmer : farmers) {
            for (Patch patch : farmer.getFarm().getPatches()) {
                patch.getGeoArea().getPatches().add(patch);
                patch.setFarmSimple(farmer.getFarm());
            }
        }
        this.dynamicsList = parameters.getDynamics();
        for (Dynamics dynamics : dynamicsList) {
            dynamics.setTimeStamp(timestamp);
            dynamics.initLogger();
        }
    }

    /**
     * Get all current products in the simulator
     *
     * @return ArrayList<Product> of products
     */
    public List<Product> getProducts() {
        Set<Product> products = new HashSet<Product>();
        for (Farmer farmer : farmers) {
            for (Patch patch : farmer.getFarm().getPatches()) {
                products.add(patch.getProduction().getProduct());
            }
        }
        for (GeoArea geoArea : geoAreas) {
            if (geoArea.getFlows() != null) {
                for (Flow flow : geoArea.getFlows()) {
                    for (Product output : flow.getOutputs()) {
                        products.add(output);
                    }
                }
            }
            for (Patch patch : geoArea.getPatches()) {
                products.add(patch.getProduction().getProduct());
            }
            if (farmers != null) {
                for (Farmer farmer : farmers) {
                    products.addAll(farmer.getFarm().getLivestock().getRatio().getProduced().keySet());
                }
            }
        }
        return new ArrayList<>(products);
    }

    /**
     * Get all current crops in the simulator
     *
     * @return ArrayList<Product> of products
     */
    public List<Product> getCrops() {
        Set<Product> products = new HashSet<Product>();
        for (Farmer farmer : farmers) {
            for (Patch patch : farmer.getFarm().getPatches()) {
                products.add(patch.getProduction().getProduct());
            }
        }
        return new ArrayList<>(products);
    }

    /**
     * Get all current transformed products in the simulator
     *
     * @return ArrayList<Product> of products
     */
    public List<Product> getTransformedProducts() {
        Set<Product> products = new HashSet<Product>();
        for (GeoArea geoArea : geoAreas) {
            if (geoArea.getFlows() != null) {
                for (Flow flow : geoArea.getFlows()) {
                    for (Product output : flow.getOutputs()) {
                        products.add(output);
                    }
                }
            }
            if (farmers != null) {
                for (Farmer farmer : farmers) {
                    products.addAll(farmer.getFarm().getLivestock().getRatio().getProduced().keySet());
                }
            }
        }
        return new ArrayList<>(products);
    }

    public List<Farmer> getFarmers() {
        return farmers;
    }

    public List<GeoArea> geoAreas() {
        return geoAreas;
    }

    public Flow getGlobal() {
        return global;
    }

    public static Simulator loadSimulator(InputStream input) throws FileNotFoundException {
        XStream xstream = createXstream();
        final Simulator simulator = (Simulator) xstream.fromXML(input);
        simulator.parameters = (Parameters) xstream.fromXML(new FileInputStream(simulator.paramsPath));
        simulator.initIgnoredFields();
        return simulator;
    }

    public void step(TimeUnit timeUnit) throws IOException {
        for (Dynamics dynamics : dynamicsList) {
            dynamics.process(context, geoAreas, farmers, global, parameters, timeUnit);
        }
        this.timestep.add(timeUnit.getValue());
    }

    public void serialize(String filename) throws IOException {
        Files.createDirectories(Paths.get("data/out/"));
        Files.createDirectories(Paths.get("data/out/" + timestamp));
        paramsPath = "data/out/" + timestamp + "/parameters.xml";
        xstream.toXML(parameters, Files.newOutputStream(Paths.get(paramsPath)));
        serializeXML(Files.newOutputStream(Paths.get("data/out/" + timestamp + "/" + filename)));
    }

    public void serializeXML(OutputStream output) {
        xstream.toXML(this, output);
    }

    public String getTimestampDirectory() {
        return "data/out/" + timestamp + "/";
    }

    public SimulationContext getContext() {
        return context;
    }

    public void patchesCBOR() throws IOException, CborException {
        CborBuilder cborBuilder = new CborBuilder();
        ArrayBuilder arrayBuilder;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        CborEncoder cborEncoder = new CborEncoder(baos);
        OutputStream os;

        arrayBuilder = cborBuilder.addArray();
        os = Files.newOutputStream(Paths.get("data/out/" + timestamp + "/patches.cbor"));

        // header
        arrayBuilder.addArray()
                .add("ID_PARCEL")
                .add("culture")
                .add("surface")
                .add("geoarea")
                .add("pratique")
                .add("production")
                .add("id_exploit")
                .end();

        for (Farmer farmer : this.getFarmers()) {
            for (Patch patch : farmer.getFarm().getPatches()) {
                arrayBuilder.addArray()
                        .add(patch.getId())
                        .add(patch.getProduction().getProduct().getLabel())
                        .add(patch.getSurface())
                        .add(patch.getGeoArea().getLabel())
                        .add(patch.getCulturalPractice().getLabel())
                        .add(patch.getProduction().getValue())
                        .add(farmer.getId())
                        .end();
            }
        }

        arrayBuilder.end();

        byte[] data = null;
        cborEncoder.encode(cborBuilder.build());

        data = baos.toByteArray();
        os.write(data);
        os.close();

    }


    public static void main(String[] args) throws Exception {
        Options options = new Options();
        Option inputFile = new Option("i", "input", true, "Simulation input file");
        inputFile.setRequired(true);
        options.addOption(inputFile);
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            loadSimulator(Files.newInputStream(Paths.get(cmd.getOptionValue("i"))));//NOPMD
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
        }
    }
}
