package fr.inrae.agriterix.simulator;

/**
 * @author loris
 */
public enum Tag {
    // examples
    A("AOP"), // plus other labels ?
    B("Bio"),
    D("Discount"),
    H("Halal"), // kacher ?
    L("Local"); // maybe remove it

    private final String label;

    Tag(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }

    public String getLabel(){
        return label;
    }
}
