package fr.inrae.agriterix.generator;

import co.nstant.in.cbor.CborException;
import com.thoughtworks.xstream.XStream;
import fr.inrae.agriterix.simulator.CulturalPractice;
import fr.inrae.agriterix.simulator.Farm;
import fr.inrae.agriterix.simulator.Farmer;
import fr.inrae.agriterix.simulator.Flow;
import fr.inrae.agriterix.simulator.GeoArea;
import fr.inrae.agriterix.simulator.Parameters;
import fr.inrae.agriterix.simulator.Patch;
import fr.inrae.agriterix.simulator.Product;
import fr.inrae.agriterix.simulator.Ratio;
import fr.inrae.agriterix.simulator.Simulator;
import fr.inrae.agriterix.simulator.Stocks;
import fr.inrae.agriterix.simulator.TimeUnit;
import fr.inrae.agriterix.simulator.Unit;
import fr.inrae.agriterix.simulator.dynamics.*;
import fr.inrae.agriterix.simulator.dynamics.transmission.TransmissionTendancy;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.opengis.feature.simple.SimpleFeature;
import umontreal.ssj.rng.MRG32k3a;
import umontreal.ssj.rng.RandomStreamBase;

public class Generator {

    /**
     * Build a list of farmers with given initial distribution of ages (starting
     * from 18 yo).
     *
     * @ages array of effectives for each ages
     * @area the GeoArea instance for initializing the farmers
     * @popTotal the total number of farmers to build.
     */
    private static List<Farmer> generate_farmers(int[] ages, GeoArea area, int popTotal) {
        int sum = Arrays.stream(ages).sum();
        double[] probas = Arrays.stream(ages).asDoubleStream().map(i -> i / sum).toArray();
        int[] population = Arrays.stream(probas).mapToInt(i -> (int) Math.round(i * popTotal)).toArray();
        List<Farmer> farmers = new ArrayList<>();
        for (int i = 0; i < population.length; i++) {
            for (int j = 0; j < population[i]; j++) {
                Farm farm = new Farm(area);
                farmers.add(new Farmer(i + 18, farm, i));
            }
        }
        return farmers;
    }

    /**
     * Randomly pick and remove a farmer in the given list
     *
     * @param rng the RNG to use
     * @param farmers the initial list of farmers
     * @param id_expl the ID to set to the picked farmer.
     * @return The farmer
     */
    private static Farmer pickFarmer(RandomStreamBase rng, List<Farmer> farmers, int id_expl) {
        Farmer farmer = farmers.remove(rng.nextInt(0, farmers.size() - 1));
        farmer.setId(id_expl);
        return farmer;
    }

    /**
     * Generates patches in the given geo area.
     *
     * @param rng The RNG used for picking the farmer
     * @param shapefile The shapefile containing the patches
     * @param area The initial area where put the patches
     * @param farmers The list of farmers to assign on the patches (unmodified)
     * @return the list of products produced by the patches
     * @throws IOException
     */
    public static List<Product> generate_patches(RandomStreamBase rng, String shapefile, GeoArea area, List<Farmer> farmers) throws IOException {
        Map<String, Product> products = new HashMap<>();
        Map<Integer, Farmer> farmersID = new HashMap<>();
        List<Patch> patches = new ArrayList<>();
        // working on a copy of farmers, because we will remove them when randomly picking
        List<Farmer> farmers_toPick = new ArrayList<>(farmers);
        // loading the shapefile
        FileDataStore dataStore = FileDataStoreFinder.getDataStore(new File(shapefile));
        SimpleFeatureSource featureSource = dataStore.getFeatureSource();
        SimpleFeatureCollection collection = featureSource.getFeatures();
        SimpleFeatureIterator iterator = collection.features();
        try {
            while (iterator.hasNext()) {
                SimpleFeature feature = iterator.next();
                int id_expl = ((Long) feature.getAttribute("id_expl")).intValue();
                String codecult = (String) feature.getAttribute("codecultco");
                Double surface = (Double) feature.getAttribute("surf_ilot");
                CulturalPractice practice = CulturalPractice.I;
                Double bio = ((Double) feature.getAttribute("BIO"));
                if (bio == 1.0) {
                    practice = CulturalPractice.O;
                }
                int patch_id = Integer.parseInt((String) feature.getAttribute("ID_PARCEL"));
                // Store the product if needed, retrieve it otherwise
                Product product;
                if (products.containsKey(codecult)) {
                    product = products.get(codecult);
                } else {
                    product = new Product(codecult, Unit.Kg);
                    products.put(codecult, product);
                }
                // Store the farmer if needed, retrieve it otherwise
                Farmer farmer;
                if (farmersID.containsKey(id_expl)) {
                    farmer = farmersID.get(id_expl);
                } else {
                    farmer = pickFarmer(rng, farmers_toPick, id_expl);
                    farmersID.put(id_expl, farmer);
                }
                // Create and add the patch to the area
                Patch patch = new Patch(area, surface.floatValue(), practice, product, farmer.getFarm(), patch_id);
                patches.add(patch);
                area.getPatches().add(patch);
            }
        } finally {
            iterator.close();
        }
        return new ArrayList<>(products.values());
    }

    /**
     * Creates a dummy flow for a given product
     *
     * @param product
     * @param maximum
     * @return
     */
    private static Flow createFlow(Product product, float maximum) {
        Stocks quantities1 = new Stocks();
        Map<Product, Float> required1 = new HashMap<>();
        Map<Product, Float> produced1 = new HashMap<>();
        required1.put(product, 1.f);
        produced1.put(product, 1.f);
        Ratio ratio1 = new Ratio(required1, produced1);
        List<Flow.Input> inputs1 = new ArrayList<>();
        List<Product> outputs1 = new ArrayList<>();
        inputs1.add(new Flow.Input(product, maximum));
        outputs1.add(product);
        return new Flow(inputs1, quantities1, outputs1, ratio1, "Commerce");

    }

    public static Map<String, Map<String, Double>> createTransMatrix(List<Product> products) throws IOException {
        // for test minimal
        XStream xStream = new XStream();
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypesByRegExp(new String[]{".*"});

        Map<String, Map<String, Double>> transitions = new ConcurrentHashMap<>();

        for (int i = 0; i < products.size(); i++) {
            Map<String, Double> transitionsP = new ConcurrentHashMap<>();
            for (int j = 0; j < products.size(); j++) {
                transitionsP.put(products.get(j).getLabel(), i == j ? 0.9 : 0.1 / (products.size() - 1));
            }
            transitions.put(products.get(i).getLabel(), transitionsP);
        }
        return transitions;
    }

    public static void main(String[] args) throws IOException, CborException {
        // farmers ages from MSA for France, starting at 18 year old
        int[] ages_2018 = {1, 76, 268, 579, 1079, 1493, 1973, 2432, 3059, 3592, 4236, 4717, 5440, 5904, 6419, 6945, 7574, 7847, 8414, 8704, 8992, 8782, 8821, 9069, 9236, 9741, 10569, 11458, 12058, 12623, 12927, 13351, 13637, 14446, 15461, 16145, 17113, 17017, 17163, 17660, 17350, 17190, 16147, 12294, 10037, 6746, 5509, 4670, 3720, 2830, 2401, 2181, 1856, 1584, 1209, 790, 665, 595, 496, 390, 333, 346, 303, 246, 238, 205, 201, 175, 146, 125, 92, 82, 75, 55, 50, 36, 36, 26, 25, 18, 13, 10, 11};
        GeoArea geoarea = new GeoArea("Puy-de-Dome");
        RandomStreamBase rng = new MRG32k3a();
        List<Farmer> farmers = generate_farmers(ages_2018, geoarea, 6160);
        List<Product> products = Generator.generate_patches(rng, "data/in/generator/patches/patches.shp", geoarea, farmers);
        for (Product product : products) {
            geoarea.getFlows().add(createFlow(product, 100.f));
        }
        List<Dynamics> dynamics = new ArrayList<>();
        dynamics.add(new PopulationDynamicsByAgeClass(
                5.31966309e-04,3.02391767e+01, 3.89377418e-01, 7.46514412e+00, 4.27098464e+01, 1.58988450e-01,
                new TransmissionTendancy(0.5, 0.5, 3, createTransMatrix(products))
        ));
        dynamics.add(new ProductionDynamics());
        dynamics.add(new MarketDynamics());
        dynamics.add(new FlowDynamics());
        dynamics.add(new StateDynamics());
        Simulator simulator = new Simulator(new Parameters(0, dynamics, "data/in/config/"), farmers);
        simulator.serializeXML(Files.newOutputStream(Paths.get("data/in/generator/simulator.xml")));
        System.out.println("# patches:  " + simulator.geoAreas().get(0).getPatches().size());
        System.out.println("# farmers:  " + simulator.getFarmers().size());
        System.out.println("# products: " + simulator.getGlobal().getInputs().size());
        System.out.println("# flows:    " + simulator.geoAreas().get(0).getFlows().size());
        for (int i = 0; i < 50; i++) {
            simulator.step(TimeUnit.YEAR);
        }
        simulator.patchesCBOR();
    }
}
