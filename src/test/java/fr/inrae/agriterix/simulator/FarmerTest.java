package fr.inrae.agriterix.simulator;

import com.thoughtworks.xstream.XStream;
import org.junit.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class FarmerTest {

    public FarmerTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSerialization() throws IOException {
        GeoArea geoArea = new GeoArea();
        Farm farm = new Farm(geoArea);

        List<Patch> patches = new ArrayList<>();
        patches.add(new Patch(geoArea, 110.4f, CulturalPractice.I, new Product("Blé", Unit.Kg),42));
        patches.add(new Patch(geoArea, 9.5f, CulturalPractice.O, new Product("Lentille", Unit.Kg), 43));
        patches.add(new Patch(geoArea, 25.3f, CulturalPractice.S, new Product("Chou", Unit.Kg), 44));
        for (Patch patch : patches) {
            patch.setFarm(farm);
        }
        Farmer farmer = new Farmer(54, farm, 1);

        XStream xstream = new XStream();
        XStream.setupDefaultSecurity(xstream);
        xstream.allowTypesByRegExp(new String[]{".*"});
        String xml = xstream.toXML(farmer);
        Farmer f = (Farmer) xstream.fromXML(xml);

        assertEquals(farmer.getAge(), f.getAge());

        List<Patch> patchesFarmer = new ArrayList<>();
        List<Patch> patchesF = new ArrayList<>();

        farmer.getFarm().patchesIterator().forEachRemaining(patchesFarmer::add);
        farmer.getFarm().patchesIterator().forEachRemaining(patchesF::add);

        Object[] pFarmer = patchesFarmer.toArray();
        Object[] pF = patchesF.toArray();

        assertArrayEquals(pFarmer, pF);

    }

}
