package fr.inrae.agriterix.simulator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.thoughtworks.xstream.XStream;
import org.junit.*;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GeoAreaTest {

    public GeoAreaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    @Test
//    public void testSerialization() throws IOException {
//        Farm farm = new Farm(new GeoArea());
//        List<Farm> farms = Collections.singletonList(farm);
//        List<Flow> flows = new ArrayList<>();
//        GeoArea g = new GeoArea(flows);
//        farm.addPatch(new Patch(g, 10.4f, CulturalPractice.I, new Product("Blé", Unit.Kg)));
//        farm.addPatch(new Patch(g, 9.5f, CulturalPractice.O, new Product("Lentille", Unit.Kg)));
//        farm.addPatch(new Patch(g, 25.3f, CulturalPractice.S, new Product("Chou", Unit.Kg)));
//        Farmer farmer = new Farmer(30, farm);
//
//        XStream xstream = new XStream();
//        XStream.setupDefaultSecurity(xstream);
//        xstream.allowTypesByRegExp(new String[]{".*"});
//        String xml = xstream.toXML(g);
//
//        GeoArea geoArea = (GeoArea) xstream.fromXML(xml);
//        for (int i = 0; i < farm.patchesSize(); i++) {
//            assertEquals(geoArea.getPatches().get(i).getSurface(), g.getPatches().get(i).getSurface());
//            assertEquals(geoArea.getPatches().get(i).getCulturalPractice(),
//                    g.getPatches().get(i).getCulturalPractice());
//            assertEquals(geoArea.getPatches().get(i).getProduction().getProduct().getLabel(),
//                    g.getPatches().get(i).getProduction().getProduct().getLabel());
//        }
//    }

    @Test
    public void testLabel() {
        GeoArea geoArea = new GeoArea("Aura");
        assertEquals("Aura", geoArea.getLabel());
        geoArea = new GeoArea(new ArrayList<>(), "Aura");
        assertEquals("Aura", geoArea.getLabel());
        geoArea = new GeoArea(new ArrayList<>(), new ArrayList<>(), "Aura");
        assertEquals("Aura", geoArea.getLabel());
    }
}