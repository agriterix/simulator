package fr.inrae.agriterix.simulator;


import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.NoTypePermission;
import fr.inrae.agriterix.simulator.dynamics.*;
import fr.inrae.agriterix.simulator.dynamics.transmission.TransmissionTendancy;
import org.junit.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class SimulatorMinimalTest {

    private static final long seed = 42;
    private static final Random random = new Random(seed);
    private String directory;

    public SimulatorMinimalTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    //@Test
    @Before
    public void setUp() throws IOException {
        // Products
        List<Tag> tagList = Arrays.asList(Tag.A, Tag.B);
        Product ble = new Product("Ble", Unit.Kg);
        Product raisin = new Product("Raisin", Unit.Kg);
        Product carottes = new Product("Carottes", Unit.Kg);
        Product pain = new Product("Pain", Unit.Kg);
        Product conserves = new Product("Conserves", Unit.Kg);
        Product vin = new Product("Vin", Unit.L);
        Product vache = new Product("Vache", Unit.A);
        Product mouton = new Product("Mouton", Unit.A);
        Product poulet = new Product("Poulet", Unit.A);
        Product lait = new Product("Lait", Unit.L);
        Product fromage = new Product("Fromage", Unit.Kg);
        ble.addTag(Tag.B);
        ble.addTag(Tag.D);
        List<Product> products = Arrays.asList(ble, raisin, carottes);
        List<Product> transformed = new ArrayList<>();
        transformed.add(pain);
        transformed.add(conserves);
        transformed.add(vin);
        List<Farm> farms = new ArrayList<>();
        List<Farmer> farmers = new ArrayList<>();
        List<Product> animals = new ArrayList<>();
        animals.add(vache);
        animals.add(mouton);
        animals.add(poulet);
        List<Patch> patches = new ArrayList<>();
        // TODO generator
        // Flows
        // Flow 1
        Stocks quantities1 = new Stocks();
        Map<Product, Float> required1 = new HashMap<>();
        Map<Product, Float> produced1 = new HashMap<>();
        required1.put(ble, 1.f);
        produced1.put(pain, 1.f);
        Ratio ratio1 = new Ratio(required1, produced1);
        List<Flow.Input> inputs1 = new ArrayList<>();
        List<Product> outputs1 = new ArrayList<>();
        inputs1.add(new Flow.Input(ble, 100.f));
        outputs1.add(pain);
        Flow flow1 = new Flow(inputs1, quantities1, outputs1, ratio1, "Boulangerie");
        // Flow 2
        Stocks quantities2 = new Stocks();
        Map<Product, Double> required2 = new HashMap<>();
        Map<Product, Double> produced2 = new HashMap<>();
        required2.put(carottes, 1.);
        produced2.put(conserves, 1.);
        Ratio ratio2 = new Ratio(required1, produced1);
        List<Flow.Input> inputs2 = new ArrayList<>();
        List<Product> outputs2 = new ArrayList<>();
        inputs2.add(new Flow.Input(carottes, 100.f));
        outputs2.add(conserves);
        Flow flow2 = new Flow(inputs2, quantities2, outputs2, ratio2, "Conserverie");
        // Flow 3
        Stocks quantities3 = new Stocks();
        Map<Product, Double> required3 = new HashMap<>();
        Map<Product, Double> produced3 = new HashMap<>();
        required3.put(raisin, 1.);
        produced3.put(vin, 1.);
        Ratio ratio3 = new Ratio(required1, produced1);
        List<Flow.Input> inputs3 = new ArrayList<>();
        List<Product> outputs3 = new ArrayList<>();
        inputs3.add(new Flow.Input(raisin, 100.f));
        outputs3.add(vin);
        Flow flow3 = new Flow(inputs3, quantities3, outputs3, ratio3, "Vignoble");
        // Flow 4
        Stocks quantities4 = new Stocks();
        Map<Product, Float> required4 = new HashMap<>();
        Map<Product, Float> produced4 = new HashMap<>();
        required4.put(lait, 1.f);
        produced4.put(fromage, 1.f);
        Ratio ratio4 = new Ratio(required4, produced4);
        List<Flow.Input> inputs4 = new ArrayList<>();
        List<Product> outputs4 = new ArrayList<>();
        inputs4.add(new Flow.Input(lait, 100.f));
        outputs4.add(fromage);
        Flow flow4 = new Flow(inputs4, quantities4, outputs4, ratio4, "Fromagerie");
        // GeoArea
        List<Flow> flows = Arrays.asList(flow1, flow2, flow3, flow4);
        GeoArea geoArea = new GeoArea(flows, patches, "minimal");
        // Farms & Farmers
        Farm farm1 = new Farm(geoArea);
        Patch patch1 = new Patch(geoArea, 100.f, CulturalPractice.S, ble, farm1, 1);
        patches.add(patch1);
        Farm farm2 = new Farm(geoArea);
        Patch patch2 = new Patch(geoArea, 100.f, CulturalPractice.I, raisin, farm2, 2);
        patches.add(patch2);
        Farm farm3 = new Farm(geoArea);
        Patch patch3 = new Patch(geoArea, 100.f, CulturalPractice.S, carottes, farm3, 3);
        patches.add(patch3);
        Farm farm4 = new Farm(geoArea);
        Map<Product, Float> requiredf4 = new HashMap<>();
        requiredf4.put(vache, 1.f);
        Map<Product, Float> producedf4 = new HashMap<>();
        producedf4.put(lait, 10.f);
        Stocks animalsf4 = new Stocks();
        animalsf4.add(vache, 10.f);
        Livestock livestock4 = new Livestock(requiredf4, producedf4, animalsf4);
        farm4.setLivestock(livestock4);
        Farmer farmer1 = new Farmer(40, farm1, 1);
        Farmer farmer2 = new Farmer(50, farm2, 2);
        Farmer farmer3 = new Farmer(60, farm3, 3);
        Farmer farmer4 = new Farmer(30, farm4, 4);
        farms.addAll(Arrays.asList(farm1, farm2, farm3, farm4));
        farmers.addAll(Arrays.asList(farmer1, farmer2, farmer3, farmer4));
        // Dynamics
        List<Dynamics> dynamics = new ArrayList<>();
        dynamics.addAll(Arrays.asList(new PopulationDynamicsByAgeClass(5.31966309e-04, 3.02391767e+01,
                        3.89377418e-01, 7.46514412e+00,4.27098464e+01, 1.58988450e-01,
                        new TransmissionTendancy(0.5, 0.5, 3,
                                createTransMatrix(products))), new ProductionDynamics(), new MarketDynamics(),
                        new FlowDynamics(), new StateDynamics()));
        Simulator simulator = new Simulator(new Parameters(0, dynamics, "data/in/config/"), farmers);
        simulator.serialize("minimal.xml");
        directory = simulator.getTimestampDirectory();
    }

    @Test
    public void testDynamics() throws IOException {
        Simulator simulator = Simulator.loadSimulator(new FileInputStream(directory + "minimal.xml"));

        int n = 100;
        for (int i = 0; i < n; i++) {
            simulator.step(TimeUnit.YEAR);
        }


    }


    public static Map<String, Map<String, Double>> createTransMatrix(List<Product> products) throws IOException {
        // for test minimal
        XStream xStream = new XStream();
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypesByRegExp(new String[]{".*"});

        Map<String, Map<String, Double>> transitions = new ConcurrentHashMap<>();

        for (int i = 0; i < products.size(); i++) {
            Map<String, Double> transitionsP = new ConcurrentHashMap<>();
            for (int j = 0; j < products.size(); j++) {
                transitionsP.put(products.get(j).getLabel(), i == j ? 0.9 : 0.1 / (products.size() - 1));
            }
            transitions.put(products.get(i).getLabel(), transitionsP);
        }
        return transitions;
    }

    //@Test
    public void createTransMatrix() throws IOException {
        // for test minimal
        XStream xStream = new XStream();
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypesByRegExp(new String[]{".*"});

        Map<Product, Map<Product, Double>> transitions = new ConcurrentHashMap<>();

        Product ble = new Product("Blé", Unit.Kg);
        Product orge = new Product("Orge", Unit.Kg);
        Product seigle = new Product("Seigle", Unit.Kg);

        //Conversions list/array
        Product[] products = {ble, orge, seigle};

        Double[][] probs = {
                {0.8, 0.1, 0.1},
                {0.2, 0.6, 0.2},
                {0.3, 0.3, 0.4}
        };

        for (int i = 0; i < products.length; i++) {
            Map<Product, Double> transitionsP = new ConcurrentHashMap<>();
            for (int j = 0; j < products.length; j++) {
                transitionsP.put(products[j], probs[i][j]);
            }
            transitions.put(products[i], transitionsP);
        }

        String xml = xStream.toXML(transitions);
        FileOutputStream fileOutputStream = new FileOutputStream("data/in/config/transitions.xml");
        fileOutputStream.write(xml.getBytes(StandardCharsets.UTF_8));
        //        FileInputStream fileInputStream = new FileInputStream("config/transitions.xml");
        //        Map<Product, Map<Product, Double>> transitionsM = (Map<Product, Map<Product, Double>>) xStream
        //                .fromXML(fileInputStream);
    }

    //@Test
    public void createProductList() throws IOException {
        XStream xStream = new XStream();
        xStream.addPermission(NoTypePermission.NONE);
        xStream.allowTypesByRegExp(new String[]{".*"});


        Product ble = new Product("Blé", Unit.Kg);
        Product carottes = new Product("Carottes", Unit.Kg);
        Product raisins = new Product("Raisins", Unit.Kg);

        //Conversions list/array
        Product[] products = {ble, carottes, raisins};
        String xml = xStream.toXML(products);
        FileOutputStream fileOutputStream = new FileOutputStream("data/in/config/products.xml");
        fileOutputStream.write(xml.getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void playground() throws FileNotFoundException {
        Simulator simulator = Simulator.loadSimulator(new FileInputStream(directory + "minimal.xml"));

        SortedMap<Double, Product> map1 = new TreeMap<>();
        Map<Product, Double> map2 = new HashMap<>();

        // 1
        map1.put(0.20, new Product("Orge", Unit.Kg));
        map1.put(0.35, new Product("Seigle", Unit.Kg));
        map1.put(0.45, new Product("Blé", Unit.Kg));

        map2.put(new Product("Orge", Unit.Kg), 0.20);
        map2.put(new Product("Seigle", Unit.Kg), 0.35);
        map2.put(new Product("Blé", Unit.Kg), 0.45);

        // 2
        //        map1.put(0.20, new Product("Patates", Unit.Kg));
        //        map1.put(0.12, new Product("Carottes", Unit.Kg));
        //        map1.put(0.08, new Product("Haricots", Unit.Kg));
        //        map1.put(0.13, new Product("Salades", Unit.Kg));
        //        map1.put(0.07, new Product("Courgettes", Unit.Kg));
        //        map1.put(0.05, new Product("Citrouilles", Unit.Kg));
        //        map1.put(0.15, new Product("Oignons", Unit.Kg));
        //        map1.put(0.14, new Product("Betteraves", Unit.Kg));
        //        map1.put(0.06, new Product("Choux", Unit.Kg));
        //
        //        map2.put(new Product("Patates", Unit.Kg), 0.20);
        //        map2.put(new Product("Carottes", Unit.Kg), 0.12);
        //        map2.put(new Product("Haricots", Unit.Kg), 0.08);
        //        map2.put(new Product("Salades", Unit.Kg), 0.13);
        //        map2.put(new Product("Courgettes", Unit.Kg), 0.07);
        //        map2.put(new Product("Citrouilles", Unit.Kg), 0.05);
        //        map2.put(new Product("Oignons", Unit.Kg), 0.15);
        //        map2.put(new Product("Betteraves", Unit.Kg), 0.14);
        //        map2.put(new Product("Choux", Unit.Kg), 0.06);

        Stocks stocks1 = new Stocks();
        Stocks stocks2 = new Stocks();

        //        System.out.println("-----------------------");
        //        System.out.println(map2);

        int n = 10;

        for (int i = 0; i < n; i++) {
            stocks1.add(simulator.getContext().nextMapObjectWithDistributionInKeys(map1), 1f / n);
            stocks2.add(simulator.getContext().nextMapObjectWithDistributionInKeysBis(map2), 1f / n);
        }

        //        System.out.println("n = " + n);
        //        System.out.println("-----------------------");
        //        System.out.println("transitions1 : " + stocks1);

        Map<Product, Double> errors1 = new HashMap<>();
        for (Map.Entry<Product, Double> entry : map2.entrySet()) {
            errors1.put(entry.getKey(), Math.abs(stocks1.get(entry.getKey()) - map2.get(entry.getKey())));
        }
        //        System.out.println("errors1: " + errors1);
        //        System.out.println("total errors 1: " + errors1.values().stream().mapToDouble(d -> d).sum());
        //        System.out.println("-----------------------");
        //        System.out.println("transitions2 : " + stocks2);

        Map<Product, Double> errors2 = new HashMap<>();
        for (Map.Entry<Product, Double> entry : map2.entrySet()) {
            errors2.put(entry.getKey(), Math.abs(stocks2.get(entry.getKey()) - map2.get(entry.getKey())));
        }

        //        System.out.println("errors2: " + errors2);
        //        System.out.println("total errors 2: " + errors2.values().stream().mapToDouble(d -> d).sum());
        //        System.out.println("-----------------------");


    }

    @After
    public void cleanup() throws IOException {
        Path current = FileSystems.getDefault().getPath("data/out");
        Files.walk(current)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }

}
