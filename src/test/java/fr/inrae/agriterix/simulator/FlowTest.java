package fr.inrae.agriterix.simulator;

import com.thoughtworks.xstream.XStream;
import org.junit.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class FlowTest {

    public FlowTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    @Test
    public void testSerialization() {
        List<Flow.Input> inputs = new ArrayList<>();
        inputs.add(new Flow.Input(new Product("Foo", Unit.Kg), 100f));
        inputs.add(new Flow.Input(new Product("Bar", Unit.L), 10000f));
        Stocks quantities = new Stocks();
        List<Product> outputType = new ArrayList<>();
        outputType.add(new Product("Manufactured", Unit.Kg));
        Ratio ratio = new Ratio(new HashMap<>(), new HashMap<>());
        Flow flow = new Flow(inputs, quantities, outputType, ratio);

        XStream xstream = Simulator.createXstream();

        Flow flow2 = (Flow) xstream.fromXML(xstream.toXML(flow));

        assertEquals(2, flow2.inputsSize());
        Flow.Input input = flow2.inputsIterator().next();
        assertEquals("Foo", input.getLabel());
        assertEquals(Unit.Kg, input.getUnit());
        assertEquals(1, flow2.outputsSize());
        assertEquals("Manufactured", flow2.outputsIterator().next().getLabel());
    }

    @Test
    public void testToString() {
        List<Flow.Input> inputs = new ArrayList<>();
        inputs.add(new Flow.Input(new Product("Foo", Unit.Kg), 100f));
        inputs.add(new Flow.Input(new Product("Bar", Unit.L), 10000f));
        Stocks quantities = new Stocks();
        List<Product> outputType = new ArrayList<>();
        outputType.add(new Product("Baz", Unit.Kg));
        Map<Product, Float> required = new HashMap<>();
        required.put(new Product("Foo", Unit.Kg), 1.f);
        required.put(new Product("Bar", Unit.L), 10.f);
        Map<Product, Float> produced = new HashMap<>();
        produced.put(new Product("Baz", Unit.Kg), 5.f);
        Ratio ratio = new Ratio(required, produced);
        Flow flow = new Flow(inputs, quantities, outputType, ratio);
        flow.toString();
    }

    @Test
    public void testTransform() {
        List<Flow.Input> inputs = new ArrayList<>();
        inputs.add(new Flow.Input(new Product("Foo", Unit.Kg), 100f));
        inputs.add(new Flow.Input(new Product("Bar", Unit.L), 10000f));
        Stocks quantities = new Stocks();
        quantities.add(new Product("Foo", Unit.Kg), 10.f);
        quantities.add(new Product("Bar", Unit.L), 100.f);
        List<Product> outputType = new ArrayList<>();
        outputType.add(new Product("Baz", Unit.Kg));
        Map<Product, Float> required = new HashMap<>();
        required.put(new Product("Foo", Unit.Kg), 1.f);
        required.put(new Product("Bar", Unit.L), 10.f);
        Map<Product, Float> produced = new HashMap<>();
        produced.put(new Product("Baz", Unit.Kg), 5.f);
        Ratio ratio = new Ratio(required, produced);
        Flow flow = new Flow(inputs, quantities, outputType, ratio);
        //System.out.println("before: " + flow.getQuantities());
        flow.produce();
        //System.out.println("after: " + flow.getQuantities());
    }

}
