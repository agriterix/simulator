package fr.inrae.agriterix.simulator;

import org.junit.*;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author loris
 */
public class LivestockTest {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testChangeLiveStock() {
        Map<Product, Float> required = new HashMap<>();
        required.put(new Product("Fourage", Unit.Kg), 100f);
        required.put(new Product("Boeuf", Unit.A), 1f);
        Map<Product, Float> produced = new HashMap<>();
        produced.put(new Product("Viande de boeuf", Unit.Kg), 30f);
        Livestock livestock = new Livestock(required, produced);
        Product boeuf = new Product("Boeuf", Unit.A);
        livestock.getAnimals().add(boeuf, 15f);
        livestock.increase(boeuf, 5);
        Float actual = livestock.getAnimals().get(boeuf);
        assertEquals(Optional.of(actual), Optional.of(20.f));
        livestock.decrease(boeuf, 4);
        actual = livestock.getAnimals().get(boeuf);
        assertEquals(Optional.of(actual), Optional.of(16.f));
    }
}
