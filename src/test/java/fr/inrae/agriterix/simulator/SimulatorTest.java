// Waiting for generator for proper configuration

package fr.inrae.agriterix.simulator;

import co.nstant.in.cbor.CborException;
import com.thoughtworks.xstream.XStream;
import fr.inrae.agriterix.simulator.dynamics.*;

import java.io.*;

import fr.inrae.agriterix.simulator.dynamics.transmission.TransmissionTendancy;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class SimulatorTest {

    private static final long seed = 42;
    private static final Random random = new Random(seed);
    private static final AtomicInteger counter = new AtomicInteger();
    private String directory = "";
    // for names
    private int s = 1;

    public SimulatorTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws IOException {
        final int PATCH_MAX = 30;
        final int FARM_ER_MAX = 6000;
        final int GEOAREA_MAX = 1;
        final int FLOW_MAX = 50;

        // Tagging products like this to prevent xstream bugs
        Product pouletTag = new Product("Poulet", Unit.A);
        pouletTag.addTag(Tag.D);
        Product boeufTag = new Product("Boeuf", Unit.A);
        boeufTag.addTag(Tag.A);
        boeufTag.addTag(Tag.H);
        Product moutonTag = new Product("Mouton", Unit.A);
        moutonTag.addTag(Tag.B);
        moutonTag.addTag(Tag.H);

        Product ble = new Product("Blé", Unit.Kg);
        Product bleT = new Product("Blé", Unit.Kg);
        bleT.addTag(Tag.B);
        bleT.addTag(Tag.D);
        Product mais = new Product("Maïs", Unit.Kg);
        Product fourrage = new Product("Fourrage", Unit.Kg);
        Product legumes = new Product("Légumes", Unit.Kg);
        Product legumesT = new Product("Légumes", Unit.Kg);
        legumesT.addTag(Tag.A);
        Product verger = new Product("Verger", Unit.Kg);
        Product vergerT = new Product("Verger", Unit.Kg);
        vergerT.addTag(Tag.B);

        List<Product> products = Arrays.asList(ble, mais, fourrage, legumes, verger, vergerT, legumesT, bleT);
        List<Product> productsL = Arrays.asList(ble, mais, fourrage, legumes, verger);

        List<Product> transformed = Arrays.asList(new Product("Jus", Unit.L), new Product("Bière", Unit.L),
                new Product("Pain", Unit.Kg), new Product("Nuggets", Unit.Kg), new Product("Soupe",
                        Unit.L), new Product("Conserves", Unit.Kg));
        List<Farm> farms = new ArrayList<>();

        List<Farmer> farmers = new ArrayList<>();

        List<Product> animals = Arrays.asList(new Product("Boeuf", Unit.A), new Product("Mouton", Unit.A),
                new Product("Poulet", Unit.A), pouletTag, boeufTag, moutonTag);

        int farmerCount = 0;

        for (int i = 0; i < GEOAREA_MAX; i++) {
            List<Patch> patches = new ArrayList<>();
            List<Flow> flows = generateFlows(products, transformed, randomInt(1, FLOW_MAX), s);
            GeoArea geoArea = new GeoArea(flows, patches, RandomStringUtils.random(8, 0, 0, true,
                    false, "abcdefghijklmnopqrstuvwxyz".toCharArray(), new Random(s++)));
            for (int j = 0; j < FARM_ER_MAX; j++) {
                Farm farm = new Farm(geoArea);
                for (int k = 0; k < PATCH_MAX; k++) {
                    Patch patch = new Patch(geoArea, randomFloat(1, 150), CulturalPractice
                            .values()[new Random().nextInt(CulturalPractice.values().length)],
                            randomProduct(products), farm, counter.getAndIncrement());
                    patches.add(patch);
                }
                Product animalChosen = animals.get(random.nextInt(animals.size()));
                Product foodChosen = farm.getPatches().get(random.nextInt(farm.patchesSize()))
                        .getProduction().getProduct();
                Map<Product, Float> required = new HashMap<>();
                required.put(animalChosen, random.nextFloat() * 10);
                required.put(foodChosen, random.nextFloat() * 100);
                Map<Product, Float> produced = new HashMap<>();
                produced.put(new Product("Viande de " + animalChosen.getLabel(), Unit.Kg), random
                        .nextFloat() * 100);
                Stocks animalsChosen = new Stocks();
                animalsChosen.add(animalChosen, random.nextFloat() * 200);
                Livestock livestock = new Livestock(required, produced, animalsChosen);
                farm.setLivestock(livestock);
                farms.add(farm);
                Farmer farmer = new Farmer(randomInt(18, 70), farm, farmerCount++);
                farmers.add(farmer);
            }
        }

        List<Dynamics> dynamics = new ArrayList<>();
        //        dynamics.add(new PopulationDynamics());
        dynamics.add(new PopulationDynamicsByAgeClass(
                5.31966309e-04, 3.02391767e+01, 3.89377418e-01, 7.46514412e+00, 4.27098464e+01, 1.58988450e-01,
                new TransmissionTendancy(0.5, 0.5, 3, createTransMatrix(productsL))
        ));
        dynamics.add(new ProductionDynamics());
        dynamics.add(new MarketDynamics());
        dynamics.add(new FlowDynamics());
        Simulator simulator = new Simulator(new Parameters(0, dynamics, "data/in/config/"),
                farmers);
        //simulator.patchesDB();
        simulator.serialize("simulator.xml");
        directory = simulator.getTimestampDirectory();
    }

    public static Map<String, Map<String, Double>> createTransMatrix(List<Product> products) throws IOException {
        // for test minimal
        XStream xStream = new XStream();
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypesByRegExp(new String[]{".*"});

        Map<String, Map<String, Double>> transitions = new ConcurrentHashMap<>();

        for (int i = 0; i < products.size(); i++) {
            Map<String, Double> transitionsP = new ConcurrentHashMap<>();
            for (int j = 0; j < products.size(); j++) {
                transitionsP.put(products.get(j).getLabel(), i == j ? 0.9 : 0.1 / (products.size() - 1));
            }
            transitions.put(products.get(i).getLabel(), transitionsP);
        }
        return transitions;
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testDynamics() throws IOException {
        Simulator simulator = Simulator.loadSimulator(new FileInputStream(directory + "simulator.xml"));
        int n = 50;
        for (int i = 0; i < n; i++) {
            simulator.step(TimeUnit.YEAR);
        }
    }

    public static List<String> loadProducts(String filename) throws IOException {
        //TODO refractor to load a products list from a file with units.
        BufferedReader abc = new BufferedReader(new FileReader(filename));
        List<String> products = new ArrayList<>();
        String line;
        while ((line = abc.readLine()) != null) {
            products.add(line);
        }
        abc.close();
        return products;
    }


    public static List<Flow> generateFlows(List<Product> products, List<Product> transformed, int flowMax, int s) {
        int inputMax = 4;
        int outputMax = 3;
        List<Flow> flows = new ArrayList<>();

        for (int i = 0; i < flowMax; i++) {

            List<Flow.Input> inputs = new ArrayList<>();
            List<Product> outputs = new ArrayList<>();

            for (int j = 0; j < inputMax; j++) {
                inputs.add(new Flow.Input(randomProduct(products), randomFloat(1, 10) * 10));
            }
            for (int j = 0; j < outputMax; j++) {
                outputs.add(randomProduct(transformed));
            }

            Stocks quantities = new Stocks();
            Map<Product, Float> required = new HashMap<>();
            Map<Product, Float> produced = new HashMap<>();

            for (Flow.Input input : inputs) {
                required.put(new Product(input.getLabel(), input.getUnit()), random.nextFloat());
            }
            for (Product output : outputs) {
                produced.put(new Product(output.getLabel(), output.getUnit()), random.nextFloat());
            }

            Ratio ratio = new Ratio(required, produced);
            flows.add(new Flow(inputs, quantities, outputs, ratio, RandomStringUtils.random(8, 0, 0, true, false,
                    "abcdefghijklmnopqrstuvwxyz".toCharArray(), new Random(s++))));
        }
        return flows;
    }

    @Test
    public void toDB() throws FileNotFoundException {
        Simulator simulator = Simulator.loadSimulator(new FileInputStream(directory + "simulator.xml"));
        try {
            simulator.patchesCBOR();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CborException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getListProducts() throws FileNotFoundException {
        Simulator simulator = Simulator.loadSimulator(new FileInputStream(directory + "simulator.xml"));
        simulator.getProducts();
        simulator.getTransformedProducts();
        simulator.getCrops();
    }

    @Test
    public void createTransMatrix() throws IOException {
        // for test minimal
        XStream xStream = new XStream();
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypesByRegExp(new String[]{".*"});

        Map<String, Map<String, Double>> transitions = new ConcurrentHashMap<>();

        String ble = "Blé";
        String mais = "Maïs";
        String fourrage = "Fourrage";
        String legumes = "Légumes";
        String verger = "Verger";

        //Conversions list/array
        String[] products = {ble, mais, fourrage, legumes, verger};

        Double[][] probs = {
                {0.4, 0.3, 0.1, 0.1, 0.1},
                {0.1, 0.2, 0.3, 0.2, 0.2},
                {0.3, 0.1, 0.2, 0.1, 0.3},
                {0.1, 0.1, 0.1, 0.2, 0.5},
                {0.1, 0.1, 0.3, 0.3, 0.2},
        };

        for (int i = 0; i < products.length; i++) {
            Map<String, Double> transitionsP = new ConcurrentHashMap<>();
            for (int j = 0; j < products.length; j++) {
                transitionsP.put(products[j], probs[i][j]);
            }
            transitions.put(products[i], transitionsP);
        }

        String xml = xStream.toXML(transitions);
        FileOutputStream fileOutputStream = new FileOutputStream("data/in/config/transitions.xml");
        fileOutputStream.write(xml.getBytes(StandardCharsets.UTF_8));
    }

    public static Product randomProduct(List<Product> products) {
        return products.get(random.nextInt(products.size()));
    }

    public static int randomInt(int i, int j) {
        return random.nextInt(j) + i;
    }

    public static Float randomFloat(int i, int j) {
        return i + (j - i) * random.nextFloat();
    }

    @After
    public void cleanup() throws IOException {
        Path current = FileSystems.getDefault().getPath("data/out");
        Files.walk(current)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }
}
