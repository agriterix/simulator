package fr.inrae.agriterix.simulator;

import fr.inrae.agriterix.simulator.dynamics.FlowDynamics;
import fr.inrae.agriterix.simulator.dynamics.MarketDynamics;
import fr.inrae.agriterix.simulator.dynamics.StateDynamics;
import org.junit.*;

import static org.junit.Assert.*;

import java.util.*;

public class DynamicsTest {

    public DynamicsTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    @Test
    public void testEditInputs() {
        List<Flow.Input> inputs = new ArrayList<>();
        inputs.add(new Flow.Input(new Product("Raisin", Unit.Kg), 100f));
        List<Product> outputs = new ArrayList<>();
        outputs.add(new Product("Vin", Unit.L));
        Map<Product, Float> required = new HashMap<>();
        required.put(new Product("Raisin", Unit.Kg), 1f);
        Map<Product, Float> produced = new HashMap<>();
        produced.put(new Product("Vin", Unit.L), 1f);
        Stocks stocks = new Stocks();
        Ratio ratio = new Ratio(required, produced);
        Flow flow = new Flow(inputs, stocks, outputs, ratio);
        FlowDynamics flowDynamics = new FlowDynamics();
        flowDynamics.initLogger();
        Map<String, Float> edits = new HashMap<>();
        edits.put("Raisin",  0.5f);
        flowDynamics.editInputs(0, flow, edits);
        assertEquals(Optional.of(flow.getInputs().get(0).getMaximum()), Optional.of(50f));
    }

    @Test
    public void testEditProduced() {
        List<Flow.Input> inputs = new ArrayList<>();
        inputs.add(new Flow.Input(new Product("Raisin", Unit.Kg), 100f));
        List<Product> outputs = new ArrayList<>();
        outputs.add(new Product("Vin", Unit.L));
        Map<Product, Float> required = new HashMap<>();
        required.put(new Product("Raisin", Unit.Kg), 1f);
        Map<Product, Float> produced = new HashMap<>();
        produced.put(new Product("Vin", Unit.L), 1f);
        Stocks stocks = new Stocks();
        Ratio ratio = new Ratio(required, produced);
        Flow flow = new Flow(inputs, stocks, outputs, ratio);
        FlowDynamics flowDynamics = new FlowDynamics();
        flowDynamics.initLogger();
        Map<String, Float> edits = new HashMap<>();
        edits.put("Vin",  0.5f);
        flowDynamics.editProduced(0, flow, edits);
        assertEquals(Optional.of(flow.getRatio().getProduced().get(new Product("Vin", Unit.L))), Optional.of(.5f));
    }

    @Test
    public void testEditRequired() {
        List<Flow.Input> inputs = new ArrayList<>();
        inputs.add(new Flow.Input(new Product("Raisin", Unit.Kg), 100f));
        List<Product> outputs = new ArrayList<>();
        outputs.add(new Product("Vin", Unit.L));
        Map<Product, Float> required = new HashMap<>();
        required.put(new Product("Raisin", Unit.Kg), 1f);
        Map<Product, Float> produced = new HashMap<>();
        produced.put(new Product("Vin", Unit.L), 1f);
        Stocks stocks = new Stocks();
        Ratio ratio = new Ratio(required, produced);
        Flow flow = new Flow(inputs, stocks, outputs, ratio);
        FlowDynamics flowDynamics = new FlowDynamics();
        flowDynamics.initLogger();
        Map<String, Float> edits = new HashMap<>();
        edits.put("Raisin",  0.5f);
        flowDynamics.editRequirements(0, flow, edits);
        assertEquals(Optional.of(flow.getRatio().getRequired().get(new Product("Raisin", Unit.Kg))), Optional.of(.5f));
    }

    @Test
    public void testAllocate() {
        // debug : String bold = "\033[0;1m";
        MarketDynamics m = new MarketDynamics();
        Product carottes = new Product("Carottes", Unit.Kg);
        Map<Product, Float> productions = new HashMap<>();
        productions.put(carottes, 234.f);
        Stocks quantities = new Stocks();
        List<Product> outputs = new ArrayList<>();
        Ratio ratio = new Ratio(new HashMap<>(), new HashMap<>());
        List<Flow.Input> inputs = new ArrayList<>();
        inputs.add(new Flow.Input(carottes, 42.f));
        Flow flow = new Flow(inputs, quantities, outputs, ratio);
        m.allocate(productions.entrySet().iterator().next(), inputs.get(0), flow, new Stocks(), 1f);
        assertEquals(Optional.of(192.f), Optional.of(productions.get(carottes)));
        assertEquals(Optional.of(42.f), Optional.of(flow.getQuantities().get(carottes)));
        List<Flow.Input> inputs2 = new ArrayList<>();
        inputs2.add(new Flow.Input(carottes, 500.f));
        Stocks quantities2 = new Stocks();
        Flow flow2 = new Flow(inputs2, quantities2, outputs, ratio);
        m.allocate(productions.entrySet().iterator().next(), inputs2.get(0), flow2, new Stocks(), 1f);
        assertEquals(Optional.of(0.f), Optional.of(productions.get(carottes)));
        assertEquals(Optional.of(192.f), Optional.of(flow2.getQuantities().get(carottes)));
    }

    @Test
    public void playground() {
    }


}
