package fr.inrae.agriterix.simulator;

import static org.junit.Assert.*;

import org.junit.*;

public class PatchTest {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testID() {
        GeoArea g = new GeoArea();
        Product p = new Product("Blé", Unit.Kg);
        Patch patch = new Patch(g, 42f, CulturalPractice.S, p, 42);
        assertEquals(42, patch.getId());
    }

    @Test
    public void testYield() {
        GeoArea g = new GeoArea();
        Product p = new Product("Blé", Unit.Kg, 2f);
        Patch patch = new Patch(g, 42f, CulturalPractice.I, p, 42);
        assertEquals(patch.getYield(), Float.valueOf(2f * 1.25f));
    }

    @Test
    public void testPracticeChangeAffectsTags() {
        Patch patch = new Patch(new GeoArea(), 420.f, CulturalPractice.O, new Product("Blé", Unit.Kg), 42);
        Product blebio = new Product("Blé", Unit.Kg);
        blebio.addTag(Tag.B);
        assert (patch.produce().containsKey(blebio));
        patch.setCulturalPractice(CulturalPractice.I);
        assert (patch.produce().containsKey(new Product("Blé", Unit.Kg)));
    }
}
