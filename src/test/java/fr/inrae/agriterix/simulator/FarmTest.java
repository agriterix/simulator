package fr.inrae.agriterix.simulator;

import org.junit.*;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class FarmTest {
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testProduce() {
        // FARM INIT
        Farm farm = new Farm(new GeoArea());
        farm.addPatch(new Patch(farm.getGeoArea(), 420.f, CulturalPractice.O, new Product("Fourage", Unit.Kg), 42));
        farm.addPatch(new Patch(farm.getGeoArea(), 420.f, CulturalPractice.O, new Product("Fourage", Unit.Kg), 43));
        farm.addPatch(new Patch(farm.getGeoArea(), 420.f, CulturalPractice.O, new Product("Blé", Unit.Kg), 44));
        farm.getLivestock().getAnimals().add(new Product("Boeuf", Unit.A), 10.f);
        // RATIO INIT
        Map<Product, Float> required = new HashMap<>();
        required.put(new Product("Fourage", Unit.Kg), 100.f);
        required.put(new Product("Boeuf", Unit.A), 1.f);
        Map<Product, Float> produced = new HashMap<>();
        produced.put(new Product("Viande de boeuf", Unit.Kg), 30.f);
        Ratio ratio = new Ratio(required, produced);
        farm.getLivestock().updateRatio(ratio);
        // ---------
        Stocks livestockBefore = new Stocks(farm.getLivestock().getAnimals());
        farm.produce();
        Stocks livestockAfter = new Stocks(farm.getLivestock().getAnimals());
        assertEquals(livestockBefore.getStocks(), livestockAfter.getStocks());
    }

    //@Test
    public void testPreventNegative() {
        // Animal based production results in negative stock of requirements.
        // FARM INIT
        Farm farm = new Farm(new GeoArea());
        farm.addPatch(new Patch(farm.getGeoArea(), 420.f, CulturalPractice.O,
                new Product("Fourage", Unit.Kg), 42));
        farm.addPatch(new Patch(farm.getGeoArea(), 420.f, CulturalPractice.O,
                new Product("Fourage", Unit.Kg), 43));
        farm.addPatch(new Patch(farm.getGeoArea(), 420.f, CulturalPractice.O,
                new Product("Blé", Unit.Kg), 44));
        farm.getLivestock().getAnimals().add(new Product("Boeuf", Unit.A), 10.f);
        // RATIO INIT
        Map<Product, Float> required = new HashMap<>();
        required.put(new Product("Fourage", Unit.Kg), 100.f);
        required.put(new Product("Boeuf", Unit.A), 1.f);
        Map<Product, Float> produced = new HashMap<>();
        produced.put(new Product("Viande de boeuf", Unit.Kg), 30.f);
        Ratio ratio = new Ratio(required, produced);
        farm.getLivestock().updateRatio(ratio);
        // ---------
        String bold = "\033[1m";
        System.out.println(bold + "INITIAL");
        System.out.println(farm.getProductions());
        farm.produce();
        System.out.println(bold + "PRODUCE");
        System.out.println(farm.getProductions());
        farm.produce();
        System.out.println(bold + "PRODUCE2");
        System.out.println(farm.getProductions());
    }

}
