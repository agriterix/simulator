package fr.inrae.agriterix.simulator;

import org.junit.*;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author loris
 */
public class ProductTest {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTagging() {
        Product product = new Product("Pomme de terre", Unit.Kg);
        product.addTag(Tag.B);
        List<Tag> tagList = new ArrayList<>();
        tagList.add(Tag.B);
        assertEquals(product.getTags(), tagList);
        product.removeTag(Tag.B);
        tagList.remove(Tag.B);
        assertEquals(product.getTags(), tagList);
    }

    @Test
    public void testTagsNotEquals() {
        Product product1 = new Product("Blé", Unit.Kg);
        product1.addTag(Tag.B);
        product1.addTag(Tag.D);
        Product product2 = new Product("Blé", Unit.Kg);
        product2.addTag(Tag.B);
        assertNotEquals(product1, product2);
    }

    @Test
    public void testTagsEquals() {
        Product product1 = new Product("Blé", Unit.Kg);
        product1.addTag(Tag.B);
        product1.addTag(Tag.D);
        Product product2 = new Product("Blé", Unit.Kg);
        product2.addTag(Tag.D);
        product2.addTag(Tag.B);
        assertEquals(product1, product2);
    }

    @Test
    public void testTagsPreserveOrder() {
        Product product = new Product("Blé", Unit.Kg);
        product.addTag(Tag.D);
        product.addTag(Tag.D);
        product.addTag(Tag.A);
        product.addTag(Tag.B);
        List<Tag> testList = new ArrayList<>();
        testList.add(Tag.A);
        testList.add(Tag.B);
        testList.add(Tag.D);
        assertEquals(product.getTags(), testList);
    }

    @Test
    public void testEqualsTagLess() {
        Product ble = new Product("Blé", Unit.Kg);
        List<Tag> tags = new ArrayList<>();
        tags.add(Tag.B);
        Product ble2 = new Product("Blé", Unit.Kg, tags);
        assert(ble.equalsTagLess(ble2));
    }

}
