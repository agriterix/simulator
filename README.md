# Agriterix simulator

This project is aiming to simulate multiple dynamics like :

- cultural practices
- agricultural production
- environmental impacts
- economic results

Further documentation on this project is 
 located [here](https://forgemia.inra.fr/agriterix/doc/).

## Usage (UNIX-like)

- Building project
    ```console
    ./gradlew build
    ```
- Testing project
    ```console
    ./gradlew test
    ```

[comment]: <> (Test files &#40;`products`, `transformed` and `simulator.xml`&#41; are located )

[comment]: <> ([here]&#40;https://stratus.irstea.fr/d/768c68c106ec4727b73a/&#41; and meant to)

[comment]: <> (be placed at the root of the project.)

To run the simulator with the input file :

```console
./gradlew run --args="-i simulator.xml"
```

This project requires Java 8 and not 11. You can force it by doing :

```console
./gradlew -Dorg.gradle.java.home=/path/to/java8 <task>
```

## TODO 

*This section might be incomplete or not up-to-date pleaser refer to the
[issues section](https://forgemia.inra.fr/agriterix/simulator/-/issues).*

- [ ] Dynamics
    - [ ] Population dynamics
        - [x] Simple installation/retirement
        - [ ] Factor-based
        - [ ] ABC-based
    - [ ] Market Dynamics
        - [x] Simple
        - [ ] Complex
    - [ ] [Acquisition dynamics](https://forgemia.inra.fr/agriterix/doc/-/blob/master/acquisition_scenarios.md) → add in `Parameters`.
    - [ ] ProductionDynamics
        - [x] Simple production
        - [ ] yield computing
- [ ] Flow loading
    - [x] Simple flow
    - [ ] Complex Flow
- [x] XML in/out    
- [x] Global seed for random choices.